import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.jnativehook.GlobalScreen
import org.jnativehook.GlobalScreen.addNativeMouseListener
import org.jnativehook.GlobalScreen.addNativeMouseMotionListener
import org.jnativehook.NativeHookException
import org.jnativehook.keyboard.NativeKeyEvent
import org.jnativehook.keyboard.NativeKeyListener
import org.jnativehook.mouse.NativeMouseEvent
import org.jnativehook.mouse.NativeMouseListener
import org.jnativehook.mouse.NativeMouseMotionListener
import java.util.logging.Level
import java.util.logging.Logger

/**
 * First attempt at using Kotlin.
 */
data class KeyState(var shiftHeld: Boolean,
                    var ctrlHeld: Boolean,
                    var altHeld: Boolean,
                    var key: Int)

data class Modifiers(var shiftHeld: Boolean,
                     var ctrlHeld: Boolean,
                     var altHeld: Boolean
                    )
{
    fun isHeld(): String
    {
        val stringBuilder = StringBuilder()
        stringBuilder.append(when {ctrlHeld -> "CTRL + "; else -> ""})
        stringBuilder.append(when {shiftHeld -> "SHIFT + "; else -> ""})
        stringBuilder.append(when {altHeld -> "ALT + "; else -> ""})
        return stringBuilder.toString()
    }
}

class InputHook
{
    companion object
    {
        var shiftHeld = false
        var ctrlHeld = false
        var altHeld = false

        var keyState = KeyState(false, false, false, 0)
        var modifiers = Modifiers(false, false, false)

        fun print(keyPressed: Int): String
        {
            var str = StringBuilder()
            if (shiftHeld)
                str.append("[SHIFT] + ")
            if (ctrlHeld)
                str.append("[CTRL] + ")
            if (altHeld)
                str.append("[ALT] + ")

            str.append(NativeKeyEvent.getKeyText(keyPressed))
            return str.toString()
        }

        fun endHook()
        {
            GlobalScreen.unregisterNativeHook()
        }

        fun startHook()
        {
            val logger = Logger.getLogger(GlobalScreen::class.java.getPackage().name)
            logger.level = Level.OFF
            try
            {
                GlobalScreen.registerNativeHook()

                addNativeMouseMotionListener(object : NativeMouseMotionListener
                {
                    override fun nativeMouseMoved(p0: NativeMouseEvent?)
                    {
                        p0 ?: return
                    }

                    override fun nativeMouseDragged(p0: NativeMouseEvent?)
                    {
                        p0 ?: return
                    }

                })

                addNativeMouseListener(object: NativeMouseListener
                {
                    override fun nativeMousePressed(p0: NativeMouseEvent?)
                    {
                        p0 ?: return
                    }

                    override fun nativeMouseClicked(p0: NativeMouseEvent?)
                    {
                        p0 ?: return
                    }

                    override fun nativeMouseReleased(p0: NativeMouseEvent?)
                    {
                        p0 ?: return
                    }
                })

                GlobalScreen.addNativeKeyListener(object : NativeKeyListener
                {

                    override fun nativeKeyTyped(nativeEvent: NativeKeyEvent)
                    {
                    }

                    override fun nativeKeyReleased(nativeEvent: NativeKeyEvent)
                    {
                    }

                    override fun nativeKeyPressed(nativeEvent: NativeKeyEvent)
                    {
                        when (nativeEvent.keyCode)
                        {
                            70 ->
                            {
                                // Send Press Key Command to Client
                            }
                        }
                        println("${nativeEvent.keyCode} | ${NativeKeyEvent.getKeyText(nativeEvent.keyCode)}")
                    }
                })
            }
            catch (e: NativeHookException)
            {
                e.printStackTrace()
            }
        }
    }
}

