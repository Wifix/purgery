import javafx.scene.image.Image
import javafx.scene.image.ImageView
import javafx.scene.layout.AnchorPane
import javafx.scene.paint.Paint
import javafx.scene.text.Text
import javafx.scene.text.TextAlignment


/**
 *
 */
class PrecedentMaker
{
    fun createPrecedent(precedent: Precedent): AnchorPane
    {
        val anchorPane = AnchorPane()

        anchorPane.prefWidth = 450.0
        anchorPane.prefHeight = 50.0
        anchorPane.style = "-fx-background: #1e1e1e"

        val txt_win = txt_format(150.0, "${precedent.accusee} v ${precedent.accused}", 150.0)
        val txt_verdict = txt_format(337.5, precedent.verdict, 75.0)
        val imgv_1 = imgv_format(Display.controller!!.suitToImage(precedent.suit1), 25.0)
        val imgv_2 = imgv_format(Display.controller!!.suitToImage(precedent.suit2), 75.0)

        anchorPane.children.addAll(txt_win, txt_verdict, imgv_1, imgv_2)

        return anchorPane
    }

    fun imgv_format(image: Image, x: Double): ImageView
    {
        val imgv = ImageView()
        imgv.image = image
        imgv.layoutY = 0.0
        imgv.layoutX = x
        imgv.fitHeight = 50.0
        imgv.fitWidth = 37.5
        return imgv
    }

    fun txt_format(x: Double, string: String, ww: Double): Text
    {
        val txt = Text()
        txt.layoutY = 25.0
        txt.text = string
        txt.wrappingWidth = ww
        txt.layoutX = x
        txt.fill = Paint.valueOf("#FFFFFF")
        txt.textAlignment = TextAlignment.CENTER
        return txt
    }
}