import javafx.application.Application
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

/**
 *
 */
class Launcher
{
    companion object
    {
        var debug = true

        @JvmStatic
        fun main(args: Array<String>)
        {
            GlobalScope.launch {
                if (debug)
                {
                    BasicServer.launchServer()
                    println("Server Done")
                }
            }

            Application.launch(Display::class.java, *args)
        }
    }
}