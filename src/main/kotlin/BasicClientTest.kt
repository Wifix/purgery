import jdk.nashorn.internal.objects.Global
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.junit.Assert.*
import org.junit.Test

/**
 *
 */
class BasicClientTest
{

    @Test
    fun sendData()
    {
        GlobalScope.launch {
            BasicServer.launchServer()
        }
        Thread.sleep(500)

        lateinit var bc: BasicClient
        GlobalScope.launch {
            bc = BasicClient()
            bc.connect()
        }

        Thread.sleep(500)
        bc.sendData("Hello\n")
    }
}