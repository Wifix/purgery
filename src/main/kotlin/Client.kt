import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.io.PrintWriter
import java.lang.Exception
import java.net.ConnectException
import java.net.Socket
import java.util.*
import kotlin.system.exitProcess

/**
 * Basic Client that will send anything typed in the console to a server.
 */
class BasicClient
/**
 * Constructor for the Basic Client.
 * @param ip Input IP, for now hardcoded to 127.0.0.1 (local).
 * @param port Input Port.
 */(private val ip: String = "213.46.195.116", private val port: Int = BasicServer.PORT_NUMBER)
{
    private var keepAlive = true
    private var server: Socket? = null
    private var output: PrintWriter? = null
    private var `in`: BufferedReader? = null

    private var UID = 0L
    var username = "DebugJoiner"

    /**
     * Sends data to server.
     * Note: /[text] is never sent.
     * @param in
     */
    fun sendData(`in`: String)
    {
        output!!.println(`in`)
        output!!.flush()
        //println("[SEND] $`in`")
    }

    /**
     * Parses commands such as: /end
     * @param in
     */
    fun commandParser(input: String)
    {
        when (input)
        {
            "/end" -> endSession()
            "/join" -> sendSRO(SERVER_RECV_OBJECT(SRO_ACTION.JOIN, userName = username))
            "/draw" -> sendSRO(SERVER_RECV_OBJECT(SRO_ACTION.DRAW, uid = UID, userName = username))
            "/turn" -> sendSRO(SERVER_RECV_OBJECT(SRO_ACTION.ENDTURN, uid = UID, userName = username))
            "/obj" -> sendSRO(SERVER_RECV_OBJECT(SRO_ACTION.OBJECTIVES, uid = UID, userName = username))
            "/players" -> sendSRO(SERVER_RECV_OBJECT(SRO_ACTION.PLAYERS, uid = UID, userName = username))
            "/nfo" -> sendSRO(SERVER_RECV_OBJECT(SRO_ACTION.INFO, uid = UID, userName = username))
            "/d1" -> sendSRO(SERVER_RECV_OBJECT(SRO_ACTION.DISCARD1, uid = UID, userName = username))
            "/d2" -> sendSRO(SERVER_RECV_OBJECT(SRO_ACTION.DISCARD2, uid = UID, userName = username))
            "/d3" -> sendSRO(SERVER_RECV_OBJECT(SRO_ACTION.DISCARD3, uid = UID, userName = username))
            "/d4" -> sendSRO(SERVER_RECV_OBJECT(SRO_ACTION.DISCARD4, uid = UID, userName = username))
            "/d5" -> sendSRO(SERVER_RECV_OBJECT(SRO_ACTION.DISCARD5, uid = UID, userName = username))
            "/d6" -> sendSRO(SERVER_RECV_OBJECT(SRO_ACTION.DISCARD6, uid = UID, userName = username))
            "/init" -> sendSRO(SERVER_RECV_OBJECT(SRO_ACTION.INIT, uid = UID, userName = username))
            "/o1" -> sendSRO(SERVER_RECV_OBJECT(SRO_ACTION.OBJ1, uid = UID, userName = username))
            "/o2" -> sendSRO(SERVER_RECV_OBJECT(SRO_ACTION.OBJ2, uid = UID, userName = username))
            "/o3" -> sendSRO(SERVER_RECV_OBJECT(SRO_ACTION.OBJ3, uid = UID, userName = username))
            "/s1" -> sendSRO(SERVER_RECV_OBJECT(SRO_ACTION.SUE1, uid = UID, userName = username))
            "/s2" -> sendSRO(SERVER_RECV_OBJECT(SRO_ACTION.SUE2, uid = UID, userName = username))
            "/s3" -> sendSRO(SERVER_RECV_OBJECT(SRO_ACTION.SUE3, uid = UID, userName = username))
            "/s4" -> sendSRO(SERVER_RECV_OBJECT(SRO_ACTION.SUE4, uid = UID, userName = username))
            "/guilty" -> sendSRO(SERVER_RECV_OBJECT(SRO_ACTION.GUILTY, uid = UID, userName = username))
            "/fight" -> sendSRO(SERVER_RECV_OBJECT(SRO_ACTION.FIGHT, uid = UID, userName = username))
            "/pwin" -> sendSRO(SERVER_RECV_OBJECT(SRO_ACTION.PWIN, uid = UID, userName = username))
            "/dwin" -> sendSRO(SERVER_RECV_OBJECT(SRO_ACTION.DWIN, uid = UID, userName = username))
            "/abstain" -> sendSRO(SERVER_RECV_OBJECT(SRO_ACTION.ABSTAIN, uid = UID, userName = username))
            "/withdraw" -> sendSRO(SERVER_RECV_OBJECT(SRO_ACTION.WITHDRAW, uid = UID, userName = username))
            "/press" -> sendSRO(SERVER_RECV_OBJECT(SRO_ACTION.PRESS, uid = UID, userName = username))
        }

    }

    /**
     * Connects with specified data.
     * debug: true to active the CLI text sending protocol
     * @throws IOException
     */
    @Throws(IOException::class)
    fun connect(debug: Boolean = false)
    {
        try
        {
            server = Socket(ip, port)
            output = PrintWriter(server!!.getOutputStream())
            `in` = BufferedReader(InputStreamReader(server!!.getInputStream()))
            println("<Connected successfully!>")
            thread_response()

            if (debug)
            {
                sendMessages()
            }
        }
        catch (ex: ConnectException)
        {
            println("Can't connect to the server.")
            ex.printStackTrace()
            throw ConnectException()
        }
    }

    val om = JacksonTools.initObjectMapper()
    private fun sendSRO(sro: SERVER_RECV_OBJECT)
    {
        try
        {
            sendData(om.writeValueAsString(sro))
        }
        catch (ex: NullPointerException)
        {
            Display.controller!!.serverOffline()
        }
    }

    /**
     * Core send messages loop.
     */
    private fun sendMessages()
    {
        val sc = Scanner(System.`in`)
        var input: String
        while (sc.nextLine().also { input = it } != null)
        {
            if (input[0] == '/')
            {
                commandParser(input)
            }
            else
            {
                sendData(input)
            }
        }
    }

    fun parseServerRespone(JSON: String)
    {
        try
        {
            val sdo = om.readValue(JSON, SERVER_DATASENDING_OBJECT::class.java)?: return

            when (true)
            {
                sdo.action == SERVER_ACTION.INFO ->
                {
                    //println("Succeful action with our UID: ${sdo.uid}")
                    println("[RECV] " + JSON)
                    PurgeryConnector.onServerAnswerSuccess(sdo)
                    UID = sdo.uid?: return
                    PurgeryConnector.connected = true
                }
                else ->
                {
                    PurgeryConnector.onServerAnswerRequestIgnored(sdo)
                    println("REJECT: ${JSON}")
                }
            }
        }
        catch (ex: Exception)
        {
            Display.controller!!.outOfDate()
        }

    }

    /**
     * Simple Thread that keeps track of everything the Server sends to the client.
     */
    fun thread_response()
    {
        val r = Runnable {
            while (keepAlive)
            {
                try
                {
                    val recv = `in`!!.readLine()
                    if (recv != null)
                        parseServerRespone(recv)

                }
                catch (e: IOException)
                {
                    e.printStackTrace()
                }
                catch (e: Exception)
                {
                    println("Server Closed Connection")
                    keepAlive = false
                    exitProcess(0)
                }
            }
        }
        val t = Thread(r)
        t.isDaemon = true
        t.start()
    }

    /**
     * Ends the loops keeping the client active.
     */
    fun endSession()
    {
        keepAlive = false
    }

}
