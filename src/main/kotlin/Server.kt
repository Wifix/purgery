import java.io.*
import java.net.ServerSocket
import java.net.Socket

/**
 * Example of a Simple Server in Java.
 */
class BasicServer private constructor(protected var socket: Socket) : Thread()
{
    val om = JacksonTools.initObjectMapper()

    fun parse(req: String): String
    {
        val sro = om.readValue(req, SERVER_RECV_OBJECT::class.java) ?: return om.writeValueAsString(SERVER_DATASENDING_OBJECT(SERVER_ACTION.REJECT))

        if (sro.action != SRO_ACTION.INFO)
            println("Server Received: $req")

        // See what actions have to be taken and do them.
        return om.writeValueAsString(createSDO(sro.action, sro.uid, sro.userName))
    }

    fun createSDO(action: SRO_ACTION, player_uid: Long?, userName: String): SERVER_DATASENDING_OBJECT
    {
        if (action == SRO_ACTION.JOIN)
            return joinPlayer(userName)

        if (player_uid == null)
        {
            return SERVER_DATASENDING_OBJECT(SERVER_ACTION.REJECT, info = "UID = null")
        }

        return when (action)
        {
            SRO_ACTION.DRAW -> drawAction(player_uid)
            SRO_ACTION.DISCARD1 -> discardAction(player_uid, 0)
            SRO_ACTION.DISCARD2 -> discardAction(player_uid, 1)
            SRO_ACTION.DISCARD3 -> discardAction(player_uid, 2)
            SRO_ACTION.DISCARD4 -> discardAction(player_uid, 3)
            SRO_ACTION.DISCARD5 -> discardAction(player_uid, 4)
            SRO_ACTION.DISCARD6 -> discardAction(player_uid, 5)
            SRO_ACTION.OBJ1 -> getObjectiveAction(player_uid, 0)
            SRO_ACTION.OBJ2 -> getObjectiveAction(player_uid, 1)
            SRO_ACTION.OBJ3 -> getObjectiveAction(player_uid, 2)
            SRO_ACTION.ENDTURN -> endTurn(player_uid)
            SRO_ACTION.OBJECTIVES -> SERVER_DATASENDING_OBJECT(SERVER_ACTION.REJECT, info = "OBJECTIVES")
            SRO_ACTION.PLAYERS -> baseSDO(player_uid)
            SRO_ACTION.INFO -> baseSDO(player_uid)

            SRO_ACTION.SUE1 -> attemptSue(player_uid, 0)
            SRO_ACTION.SUE2 -> attemptSue(player_uid, 1)
            SRO_ACTION.SUE3 -> attemptSue(player_uid, 2)
            SRO_ACTION.SUE4 -> attemptSue(player_uid, 3)

            SRO_ACTION.GUILTY -> pleadGuilty(player_uid)
            SRO_ACTION.FIGHT -> fight(player_uid)
            SRO_ACTION.PWIN -> { bet(player_uid, 1); return baseSDO(player_uid) }
            SRO_ACTION.DWIN -> { bet(player_uid, 2); return baseSDO(player_uid) }
            SRO_ACTION.ABSTAIN -> { bet(player_uid, 0); return baseSDO(player_uid) }
            SRO_ACTION.WITHDRAW -> { PurgeryGame.withdraw(player_uid); baseSDO(player_uid)}
            SRO_ACTION.PRESS -> {PurgeryGame.press(player_uid); baseSDO(player_uid)}

            SRO_ACTION.INIT ->
            {
                PurgeryGame.init()
                baseSDO(player_uid)
            }
            else -> SERVER_DATASENDING_OBJECT(SERVER_ACTION.REJECT, info = "OTHER")
        }
    }

    /**
     * 0 abstain
     * 1 pwin
     * 2 dwin
     */
    fun bet(playerUID: Long, n: Int)
    {
        when (n)
        {
            0 -> PurgeryGame.abstain(playerUID)
            1 -> PurgeryGame.pWin(playerUID)
            2 -> PurgeryGame.dWin(playerUID)
        }
    }

    fun fight(playerUid: Long): SERVER_DATASENDING_OBJECT
    {
        return if (PurgeryGame.isAccused(playerUid))
        {
            PurgeryGame.fightAcc(playerUid)
            baseSDO(playerUid)
        }
        else
            SERVER_DATASENDING_OBJECT(SERVER_ACTION.REJECT, info = "You are not the defendant")
    }

    fun pleadGuilty(playerUid: Long): SERVER_DATASENDING_OBJECT
    {
        return if (PurgeryGame.isAccused(playerUid))
        {
            PurgeryGame.pleadGuilty(playerUid)
            baseSDO(playerUid)
        }
        else
            SERVER_DATASENDING_OBJECT(SERVER_ACTION.REJECT, info = "You are not the Defendant")
    }

    fun attemptSue(playerUid: Long, n: Int): SERVER_DATASENDING_OBJECT
    {
        return if (PurgeryGame.canSue(playerUid, n))
        {
            PurgeryGame.sue(playerUid, n)
            baseSDO(playerUid)
        }
        else
            SERVER_DATASENDING_OBJECT(SERVER_ACTION.REJECT, info = "Your sue attempt failed.")
    }

    fun getObjectiveAction(playerUid: Long, n: Int): SERVER_DATASENDING_OBJECT
    {
        if (PurgeryGame.attemptToGetObj(n, playerUid))
            return baseSDO(playerUid)
        return SERVER_DATASENDING_OBJECT(SERVER_ACTION.REJECT, info = "You can't get this objective at this time!")
    }

    private fun discardAction(playerUid: Long, n: Int): SERVER_DATASENDING_OBJECT
    {
        return if (PurgeryGame.discardCard(playerUid, n))
        {
            baseSDO(playerUid)
        }
        else
        {
            SERVER_DATASENDING_OBJECT(SERVER_ACTION.REJECT, info = "Can't discard at this moment")
        }
    }

    fun endTurn(uid: Long): SERVER_DATASENDING_OBJECT
    {
        return when
        {
            PurgeryGame.endTurn(uid) -> {baseSDO(uid)}
            else -> {SERVER_DATASENDING_OBJECT(SERVER_ACTION.REJECT, info = "It's not your turn!")}
        }
    }

    fun baseSDO(uid: Long): SERVER_DATASENDING_OBJECT
    {
        return  SERVER_DATASENDING_OBJECT(
                SERVER_ACTION.INFO,
                PurgeryGame.getGAMEINFO(),
                PurgeryGame.getGamePrecedents(),
                PurgeryGame.latestLawSuitStatus(),
                PurgeryGame.objectives,
                PurgeryGame.currentTurn,
                PurgeryGame.getHand(uid),
                PurgeryGame.getNames(uid),
                null,
                "",
                PurgeryGame.latestGameState(),
                uid,
                PurgeryGame.market)
    }

    fun drawAction(uid: Long): SERVER_DATASENDING_OBJECT
    {
        val c = PurgeryGame.drawCard(uid)

        return if (c == null)
            SERVER_DATASENDING_OBJECT(SERVER_ACTION.REJECT, info = "You can't draw right now.")
        else
            SERVER_DATASENDING_OBJECT(
                    SERVER_ACTION.INFO,
                    PurgeryGame.getGAMEINFO(),
                    PurgeryGame.getGamePrecedents(),
                    PurgeryGame.latestLawSuitStatus(),
                    PurgeryGame.objectives,
                    PurgeryGame.currentTurn,
                    PurgeryGame.getHand(uid),
                    PurgeryGame.getNames(uid),
                    PurgeryGame.drawnCard,
                    "",
                    PurgeryGame.latestGameState(),
                    uid,
                    PurgeryGame.market)

    }

    fun joinPlayer(userName: String): SERVER_DATASENDING_OBJECT
    {
        // Create a new player and send over the new UID with all relevant info.
        if (PurgeryGame.playerCanJoin())
        {
            val p = PurgeryGame.playerJoinRequest(userName)
            return if (p == null)
            {
                SERVER_DATASENDING_OBJECT(SERVER_ACTION.REJECT)
            }
            else
            {
                SERVER_DATASENDING_OBJECT(
                        SERVER_ACTION.INFO,
                        PurgeryGame.getGAMEINFO(),
                        PurgeryGame.getGamePrecedents(),
                        PurgeryGame.latestLawSuitStatus(),
                        PurgeryGame.objectives,
                        PurgeryGame.currentTurn,
                        PurgeryGame.getHand(p.uid),
                        PurgeryGame.getNames(p.uid),
                        PurgeryGame.drawnCard,
                        "",
                        PurgeryGame.latestGameState(),
                        p.uid)
            }
        }
        return SERVER_DATASENDING_OBJECT(SERVER_ACTION.REJECT)
    }

    /**
     * Launches the server, and prints all data received to the console.
     */
    override fun run()
    {
        var `in`: InputStream? = null
        var out: OutputStream? = null
        try
        {
            `in` = socket.getInputStream()
            out = socket.getOutputStream()
            val br = BufferedReader(InputStreamReader(`in`))
            var request: String
            while (br.readLine().also { request = it } != null)
            {
                request = parse(request)
                // This \n is really important, without the linebreak (\n) there is nothing written to the client.
                request += '\n'
                out.write(request.toByteArray())
            }
        }
        catch (ex: IOException)
        {
            println("Unable to get streams from client")
        }
        finally
        {
            try
            {
                `in`!!.close()
                out!!.close()
                socket.close()
            }
            catch (ex: IOException)
            {
                ex.printStackTrace()
            }
        }
    }

    companion object
    {
        var PORT_NUMBER = 8120
        /**
         * Method used for launching and activating a server.
         * @param args Launch arguments.
         */
        @JvmStatic
        fun launchServer(PORT: Int = 8120)
        {
            if (PORT in 8080..65535)
                PORT_NUMBER = PORT

            println("Launching server...")
            var server: ServerSocket? = null
            try
            {
                server = ServerSocket(PORT_NUMBER)
                println("Server started, listening on $PORT_NUMBER")
                PurgeryGame.init()
                while (true)
                {
                    /**
                     * create a new [SocketServer] object for each connection
                     * this will allow multiple client connections.
                     */
                    BasicServer(server.accept())
                }
            }
            catch (ex: IOException)
            {
                // User disconnected, free up the spot in the game they occupied!
                println("Unable to start server.")
                ex.printStackTrace()
            }
            finally
            {
                try
                {
                    server?.close()
                }
                catch (ex: IOException)
                {
                    ex.printStackTrace()
                }
            }
        }
    }

    /**
     * Constructor.
     * @param socket Input a socket.
     */
    init
    {
        println("New client connected from " + socket.inetAddress.hostAddress)
        start()
    }
}
