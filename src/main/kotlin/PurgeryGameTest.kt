import org.junit.Test

import org.junit.Assert.*
import java.util.*

/**
 *
 */
class PurgeryGameTest
{
    @Test
    fun testUID()
    {
        fillGameLobby()
        assertTrue(PurgeryGame.players[0]!!.uid in 100000000000000      until 999999999999999L)
        assertTrue(PurgeryGame.players[1]!!.uid in 1000000000000000     until 9999999999999999L)
        assertTrue(PurgeryGame.players[2]!!.uid in 10000000000000000    until 99999999999999999L)
        assertTrue(PurgeryGame.players[3]!!.uid in 100000000000000000   until 999999999999999999L)
    }

    @Test
    fun endgameTest()
    {
        fillGameLobby()

        PurgeryGame.hands[0]!!.cards = arrayListOf(Card(Suits.FISH, 1, Effects.NONE), null, null, null, null)
        PurgeryGame.hands[1]!!.cards = arrayListOf(null, null, null, null, null)
        PurgeryGame.hands[2]!!.cards = arrayListOf(null, null, null, null, null)
        PurgeryGame.hands[3]!!.cards = arrayListOf(null, null, null, null, null)

        PurgeryGame.drawCard(PurgeryGame.players[0]!!.uid)
        PurgeryGame.discardCard(PurgeryGame.players[0]!!.uid, 0)
        PurgeryGame.endTurn(PurgeryGame.players[0]!!.uid)

        assertTrue(PurgeryGame.state == 2)
    }

    @Test
    fun faderTest()
    {
        PurgeryGame.courtRoomFader(12)
        while(true)
        {
            Thread.sleep(12*1000)
        }
    }

    @Test
    fun turnSkipTest()
    {
        fillGameLobby()

        PurgeryGame.hands[0]!!.cards = arrayListOf(Card(Suits.FISH, 1, Effects.NONE), null, null, null, null)
        PurgeryGame.hands[1]!!.cards = arrayListOf(null, null, null, null, null)
        PurgeryGame.hands[2]!!.cards = arrayListOf(Card(Suits.FISH, 1, Effects.NONE), null, null, null, null)
        PurgeryGame.hands[3]!!.cards = arrayListOf(null, null, null, null, null)

        PurgeryGame.drawCard(PurgeryGame.players[0]!!.uid)
        PurgeryGame.discardCard(PurgeryGame.players[0]!!.uid, 0)
        PurgeryGame.endTurn(PurgeryGame.players[0]!!.uid)

        assertTrue(PurgeryGame.currentTurn == 2)
    }

    @Test
    fun testCardsInhand()
    {
        fillGameLobby()
    }

    @Test
    fun attemptToGetAnObjective()
    {
        fillGameLobby()
        println(Arrays.toString(PurgeryGame.objectives[0]))
        PurgeryGame.hands[0]!!.cards[0]!!.suit = PurgeryGame.objectives[0]!![0]
        PurgeryGame.hands[0]!!.cards[1]!!.suit = PurgeryGame.objectives[0]!![1]
        PurgeryGame.hands[0]!!.cards[2]!!.suit = PurgeryGame.objectives[0]!![2]

        // Hand has the cards for a bonus obj
        println(Arrays.toString(PurgeryGame.hands[0]!!.cards.toArray()))

        assertTrue(PurgeryGame.attemptToGetObj(0, PurgeryGame.players[0]!!.uid))
        println("OBJECTIVE GET")

        // Hand should have changed with new cards
        println(Arrays.toString(PurgeryGame.hands[0]!!.cards.toArray()))
    }

    @Test
    fun lawsDeck()
    {
        PurgeryGame.createLawsDeck()
        println(Arrays.toString(PurgeryGame.lawsDeck.toArray()))
    }

    @Test
    fun playerJoinRequest()
    {
        // Server gets Client Request to Join the game -->
        // Server asks the Game Object if it has a spot...
        val result = PurgeryGame.playerCanJoin()
        if (result)
        {
            val p = PurgeryGame.playerJoinRequest("Riz")
            if (p == null)
            {
                println("Game rejected a join")
                assertFalse("Rejected connection", true)
            }
            else
            {
                println(PurgeryGame.players.contentToString())
                assertTrue(p.name == "Riz")
            }
        }
        else
            assertFalse("Cannot find a spot", true)
    }

    fun fillGameLobby()
    {
        PurgeryGame.init()

        for (c in 0..3)
        {
            val p = PurgeryGame.playerJoinRequest("Riz $c")
        }
        println(PurgeryGame.players.contentToString())
    }

    @Test
    fun playerJoinRequestUntilFail()
    {
        // Server gets Client Request to Join the game -->
        // Server asks the Game Object if it has a spot...
        val result = PurgeryGame.playerCanJoin()

        for (c in 0..4)
        {
            if (result)
            {
                val p = PurgeryGame.playerJoinRequest("Riz")
                if (p == null)
                {
                    assertNull(p)
                }
                println(PurgeryGame.players.contentToString())
            }
        }
    }
}