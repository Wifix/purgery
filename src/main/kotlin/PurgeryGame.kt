import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.time.Duration
import java.util.*
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.atomic.AtomicInteger
import kotlin.collections.ArrayList
import kotlin.math.ln
import kotlin.math.pow

/**
 *
 */
object PurgeryGame
{
    /**
     * 1 - Active Game
     * 2 - No More Moves for any players.
     */
    var state = 1
    var currentTurn = 0
    var cardDeck = Collections.synchronizedCollection(ArrayList<Card>())
    var turnUIDmap = ConcurrentHashMap<Long, Int>()
    var cardUID = 0
    var coins = ConcurrentHashMap<Long, Int>()

    var players: Array<Player?> = arrayOf(null, null, null, null)
    var hands = arrayOfNulls<Hand>(4)
    var market = ArrayList<Card>()
    var objectives = HashMap<Int, Array<Suits>>()

    var gameINFO = "Waiting for Player 1 to draw"
    var hasDrawn = false
    var hasDiscarded = false
    var lawsDeck = ArrayList<YOUR_LAW>()

    var lawSuit = false
    var plaintiffName = ""
    var defendantName = ""

    var betorsDecide = true

    var precedents = PRECEDENTS(ArrayList())
    /**
     * -1 : Not in a lawsuit
     * 0  : Player X has started a suit against Player Y
     * 1  : Player Y can submit or fight
     * 2  : Players V & W can bet on the case
     * 3  : Player X can withdraw or call
     * 4  : Defendant INNOCENT
     * 6  : Defendant GUILTY
     * 5  : Defendant INNOCENT (Plaintiff Perjury)
     */

    var plaintiffStake = AtomicInteger(0)
    var defendantStake = AtomicInteger(0)

    fun lawSuitBlocker(): Boolean = lawSuitState != -1

    var lawSuitState = -1

    fun dLose()
    {
        val c2 = market.last()

        defendantLost = true
        val d = players[turnUIDmap[defendantUID]!!] ?: return
        val p = players[turnUIDmap[plaintiffUID]!!] ?: return

        d.coins--
        p.coins++
        p.coins++

        for (map in bets.entries)
        {
            val uid = map.key
            val bet = map.value
            if (bet == 1)
                players[turnUIDmap[uid]!!]!!.coins += 1
            else if (bet == 2 && c2.effect.effectID == 9)
                players[turnUIDmap[uid]!!]!!.coins -= 1
        }
        bets = ConcurrentHashMap()

        val c1 = market[market.lastIndex-1]
        precedents.list.add(Precedent(plaintiffName, defendantName, c1.suit, c2.suit, "GUILTY"))

        c2.effect.resolve()

        courtRoomFader(12)
    }

    fun dWin()
    {
        val c2 = market.last()

        defendantLost = false
        val d = players[turnUIDmap[defendantUID]!!] ?: return
        val p = players[turnUIDmap[plaintiffUID]!!] ?: return

        d.coins++
        d.coins++

        p.coins--

        for (map in bets.entries)
        {
            val uid = map.key
            val bet = map.value
            if (bet == 2)
                players[turnUIDmap[uid]!!]!!.coins+=1
            else if (bet == 1 && c2.effect.effectID == 9)
                players[turnUIDmap[uid]!!]!!.coins -= 1
        }
        bets = ConcurrentHashMap()


        val c1 = market[market.lastIndex-1]
        precedents.list.add(Precedent(plaintiffName, defendantName, c1.suit, c2.suit, "INNOCENT"))

        c2.effect.resolve()

        courtRoomFader(12)
    }

    var opacity = 1.0
    fun courtRoomFader(duration: Long)
    {
        var mils: Long = 0
        GlobalScope.launch {
            while (mils <= duration*1000)
            {
                delay(100)
                mils += 100
            }
            lawSuitState = -1
        }
    }

    var lawVerified = false

    fun withdraw(uid: Long)
    {
        if (isPlaintiff(uid))
        {
            dWin()

            lawSuitState = 5
        }
    }

    fun press(uid: Long)
    {
        if (isPlaintiff(uid))
        {
            lawVerified = true

            // Check if the law that the player has is equal to what it was.
            val last_card_from_market = market.last()
            val second_last_card_from_market = market[market.lastIndex-1]

            // Decide win or lose
            val lawcardLeft = hands[turnUIDmap[uid]!!]!!.lawCard!!.suit1
            val lawcardRight = hands[turnUIDmap[uid]!!]!!.lawCard!!.suit2

            // FELONY Handler
            when (last_card_from_market.effect.effectID)
            {
                7 ->
                {
                    if (lawcardLeft == second_last_card_from_market.suit)
                    {
                        if (lawcardRight == last_card_from_market.suit || lawcardRight == second_last_card_from_market.suit)
                        {
                            dLose()
                            lawSuitState = 6
                        }
                    }
                    else
                    {
                        dWin()
                        lawSuitState = 5
                    }
                }
                else ->
                {
                    if (lawcardLeft == second_last_card_from_market.suit && lawcardRight == last_card_from_market.suit)
                    {
                        dLose()
                        lawSuitState = 6
                    }
                    else
                    {
                        dWin()
                        lawSuitState = 5
                    }
                }
            }

            // Get new law card
            val discarded = hands[turnUIDmap[plaintiffUID]!!]!!.lawCard!!
            val newCard = lawsDeck.random()
            lawsDeck.remove(newCard)
            lawsDeck.add(discarded)
            hands[turnUIDmap[plaintiffUID]!!]!!.lawCard = newCard

            lawVerified = false
        }
    }

    fun discardAndDrawLawCard(uid: Long)
    {
        val discarded = hands[turnUIDmap[uid]!!]!!.lawCard!!
        val newCard = lawsDeck.random()
        lawsDeck.remove(newCard)
        lawsDeck.add(discarded)
        hands[turnUIDmap[uid]!!]!!.lawCard = newCard
    }


    /**
     * UID, OPTION
     * 0 -> Abstain
     * 1 -> Plaintiff Win
     * 2 -> Defendant Win
     */
    var bets = ConcurrentHashMap<Long, Int>()

    fun abstain(uid: Long): Boolean
    {
        if (isPlaintiff(uid) || isAccused(uid))
            return false

        return if (!bets.containsKey(uid))
        {
            bets[uid] = 0
            true
        }
        else
            false
    }

    fun dWin(uid: Long): Boolean
    {
        if (isPlaintiff(uid) || isAccused(uid))
            return false

        return if (!bets.containsKey(uid))
        {
            bets[uid] = 2
            defendantStake.incrementAndGet()
            true
        }
        else
            false
    }

    fun pWin(uid: Long): Boolean
    {
        if (isPlaintiff(uid) || isAccused(uid))
            return false



        return if (!bets.containsKey(uid))
        {
            bets[uid] = 1
            plaintiffStake.incrementAndGet()
            true
        }
        else
            false
    }

    fun fightAcc(uid: Long)
    {
        if (isAccused(uid))
        {
            defendantStake.incrementAndGet()
            lawSuitState = 2
            betThread()
        }
    }

    fun betThread()
    {
        GlobalScope.launch {
            var done = true
            while (done)
            {
                if (bets.size >= 2)
                {
                    done = false
                }
                delay(100)
            }

            if (betorsDecide)
            {
                if (defendantStake.get() >= 4)
                {
                    dWin()
                    lawSuitState = 4
                    return@launch
                }
                else if (plaintiffStake.get() >= 3)
                {
                    dLose()
                    lawSuitState = 6
                    return@launch
                }
            }
            lawSuitState = 3
        }

    }

    /**
     * No lawsuit, player gives over points
     */
    fun pleadGuilty(uid: Long): Boolean
    {
        return when
        {
            isAccused(uid) ->
            {
                lawVerified = false
                lawSuitState = 6
                dLose()
                true
            }
            else ->
            {
                false
            }
        }
    }

    fun isAccused(uid: Long): Boolean = defendantUID == uid
    fun isPlaintiff(uid: Long): Boolean = plaintiffUID == uid

    fun canSue(uid: Long, target: Int) = if (market.size>=2)
        turnUIDmap[uid] != target
    else
        false

    var plaintiffUID = 0L
    var defendantUID = 0L

    fun sue(uid: Long, target: Int)
    {
        if (lawSuitBlocker())
            return



        if (canSue(uid, target))
        {
            val c2 = market.last()

            if (c2.effect.effectID == 3)
                return

            lawSuit = true
            lawSuitState = 0
            plaintiffName = players[turnUIDmap[uid]!!]!!.name
            plaintiffUID = uid

            plaintiffStake.incrementAndGet()
            defendantStake.incrementAndGet()

            defendantName = if (players[target] == null)
                "?"
            else
                players[target]!!.name
            defendantUID = players[target]!!.uid
        }
    }

    fun addCopyToObjDeck(card: Card)
    {
        objectivesDeck.add(card)
        objectivesDeck.shuffle()
    }

    /**
     * Rests all info related to a game, so that a game is active.
     */
    fun init()
    {
        lawSuitState = -1
        currentTurn = 0
        state = 1
        cardDeck = Collections.synchronizedCollection(ArrayList<Card>())
        cardUID = 0
        market = ArrayList()
        precedents = PRECEDENTS(ArrayList())
        objectivesDeck = ArrayList()
        plaintiffStake.set(0)
        defendantStake.set(0)
        opacity = 1.0

        // Allot card draws
        /*
        fillDeckWith(10, Suits.FISH)
        fillDeckWith(10, Suits.BAKING)
        fillDeckWith(10, Suits.CONSTRUCTION)
        fillDeckWith(10, Suits.HARVEST)
        fillDeckWith(10, Suits.LIVESTOCK)
        */
        fillDeckWith(10, Suits.FISH)
        fillDeckWith(10, Suits.BAKING)
        addEffects()
        /*
        fillDeckWith(3, Suits.FISH2)
        fillDeckWith(3, Suits.BAKING2)
        fillDeckWith(3, Suits.CONSTRUCTION2)
        fillDeckWith(3, Suits.HARVEST2)
        fillDeckWith(3, Suits.LIVESTOCK2)
        fillDeckWith(2, Suits.FISH3)
        fillDeckWith(2, Suits.BAKING3)
        fillDeckWith(2, Suits.CONSTRUCTION3)
        fillDeckWith(2, Suits.HARVEST3)
        fillDeckWith(2, Suits.LIVESTOCK3)*/

        cardDeck = cardDeck.shuffled()

        println(cardDeck.toTypedArray().contentToString())

        generateObjectives()
        println("Generated Objectives: ${objectives}")

        // draw cards later give them to a player
        putCardsInHands()

        players
                .asSequence()
                .filterNotNull()
                .forEach { it.coins = 3 }

        println(hands.contentToString())
        hasDrawn = false
        hasDiscarded = false

        gameINFO = "Waiting for Player 1 to draw"

    }

    private fun addEffects()
    {
        var cache = Effects.PROTECTION
        cardDeck.map {
            card: Card? -> card!!.effect = cache.next(); cache = cache.next()
        }
    }


    fun generateObjectives()
    {
        setObjective(0, doubleSingle())
        setObjective(1, rngObj())
        setObjective(2, sameObj())
        /*
        for (c in 0..2)
        {
            setObj(c)
        }*/
    }

    fun setObjective(n: Int, suits: Array<Suits>)
    {
        objectives[n] = suits
    }

    fun setObj(n: Int)
    {
        val gen = generateObjective()
        if (objectives.containsValue(gen))
            setObj(n)
        else
        {
            objectives[n] = gen
            return
        }
    }

    fun generateObjective(): Array<Suits>
    {
        return when (Random().nextInt(100))
        {
            in 0..45 -> doubleSingle()
            in 46..90 -> sameObj()
            else -> rngObj()
        }
    }

    var objectivesDeck = ArrayList<Card>()

    fun doubleSingle(): Array<Suits>
    {
        val c = objectivesDeck.random()
        val c2 = objectivesDeck.random()
        return arrayOf(c.suit, c.suit, c2.suit)
    }

    fun sameObj(): Array<Suits>
    {
        val c = objectivesDeck.random()
        return arrayOf(c.suit, c.suit, c.suit)
    }

    fun rngObj(): Array<Suits>
    {
        return arrayOf(objectivesDeck.random().suit, objectivesDeck.random().suit, objectivesDeck.random().suit)
    }


    @Deprecated("Uses all types of cards")
    fun genObj(): Array<Suits>
    {
        return when (Random().nextInt(100))
        {
            in 0..20 -> verifyUniqueness(doubleSingleObj())
            in 21..40 -> verifyUniqueness(sameObjective())
            in 41..89 -> verifyUniqueness(sequentialObjective())
            else -> verifyUniqueness(pureRandomObj())
        }
    }

    fun verifyUniqueness(arr: Array<Suits>): Array<Suits>
    {
        for (ob in objectives)
        {
            if (ob == arr)
                return genObj()
        }
        return arr
    }


    fun sequentialObjective(): Array<Suits>
    {
        val rng = Random().nextInt(4)
        return when (rng)
        {
            0 -> arrayOf(Suits.FISH, Suits.FISH2, Suits.FISH3)
            1 -> arrayOf(Suits.BAKING, Suits.BAKING2, Suits.BAKING3)
            2 -> arrayOf(Suits.LIVESTOCK, Suits.LIVESTOCK2, Suits.LIVESTOCK3)
            3 -> arrayOf(Suits.HARVEST, Suits.HARVEST2, Suits.HARVEST3)
            else -> arrayOf(Suits.CONSTRUCTION, Suits.CONSTRUCTION2, Suits.CONSTRUCTION3)
        }
    }

    fun sameObjective(): Array<Suits>
    {
        val array = Suits.FISH.fullArray()
        val single = array.random()

        return when (single)
        {
            Suits.FISH3 -> sameObjective()
            Suits.BAKING3 -> sameObjective()
            Suits.CONSTRUCTION3 -> sameObjective()
            Suits.LIVESTOCK3 -> sameObjective()
            Suits.HARVEST3 -> sameObjective()
            else -> arrayOf(single, single, single)
        }

    }

    fun doubleSingleObj(): Array<Suits>
    {
        val array = Suits.FISH.fullArray()

        val double = array.random()
        array.remove(double)
        val single = array.random()

        return arrayOf(double, double, single)
    }

    fun pureRandomObj(): Array<Suits>
    {
        val res = ArrayList<Suits>()
        for (c in 0..3)
        {
            res.add(Suits.FISH.Random())
        }
        // Check if the arraylist contains illegal objectives
        val set = HashSet<Suits>(res)
        if (set.size == 1)
        {
            when (res.removeAt(0))
            {
                Suits.FISH3 -> res.add(Suits.FISH2)
                Suits.BAKING3 -> res.add(Suits.BAKING2)
                Suits.CONSTRUCTION3 -> res.add(Suits.CONSTRUCTION2)
                Suits.LIVESTOCK3 -> res.add(Suits.LIVESTOCK2)
                Suits.HARVEST3 -> res.add(Suits.HARVEST2)
            }
        }
        return res.toTypedArray()
    }



    fun putCardsInHands()
    {
        createLawsDeck()
        for (i in 0 until 4)
        {
            val lc = lawsDeck.random()
            lawsDeck.remove(lc)
            hands[i] = Hand(ArrayList(), lc)
        }
        for (c in 0 until 20)
        {
            val card = cardDeck.random()
            hands[c%4]!!.cards.add(card)
            cardDeck.remove(card)
        }
    }

    fun fillDeckWith(max: Int, suit: Suits)
    {
        for (c in 0 until max)
            cardDeck.add(Card(suit, cardUID++, Effects.NONE))

        addCopyToObjDeck(Card(suit, -1, Effects.NONE))
    }

    fun playerJoinRequest(playerName: String): Player?
    {
        if (!playerCanJoin()) return null

        var turnIndicator = 0
        for (c in 3 downTo 0)
        {
            if (players[c] == null)
                turnIndicator = c
        }

        val p = Player(Hand(ArrayList(), null), generateUID(turnIndicator.toLong()), playerName, 3)
        players[turnIndicator] = p
        turnUIDmap[p.uid] = turnIndicator

        gameINFO = "$playerName has joined the game!"

        return p
    }

    /**
     * Generates a random number of 15+turn digits.
     * +turn to always ensure the number is new.
     */
    fun generateUID(turnIndicator: Long): Long
    {
        var uid = 0L
        for (c:Long in 0L..14L+turnIndicator)
            uid += ( (10.toDouble().pow(c.toDouble()).toLong()) * (Random().nextInt(8)+1))
        return uid
    }

    fun playerCanJoin():Boolean
    {
        return when
        {
            players.contains(null) -> true
            else -> false
        }
    }

    fun isPlayerTurn(player_uid: Long): Boolean
    {
        players[currentTurn] ?: return false
        return players[currentTurn]!!.uid == player_uid
    }

    fun getTurn(): Int
    {
        return currentTurn
    }

    fun getHand(player_uid: Long): Hand?
    {
        val turn = turnUIDmap[player_uid] ?: return null
        return hands[turn]
    }

    /**
     * Handles the discard pile, if it's greater than 44 as a result of adding a card it will resize the pile back
     * to 2
     */
    fun addCardToMarket(card: Card?)
    {
        if (card != null)
            market.add(card)
    }


    fun discardCard(player_uid: Long, cardNum: Int): Boolean
    {
        if (lawSuitBlocker())
            return false

        if (isPlayerTurn(player_uid))
        {
            if (hasDiscarded)
            {
                println("!!!!!!!!!!! HAS DISCARDED !!!!!!!!!!!")
                gameINFO = "Waiting for Player ${currentTurn+1} to end their turn."
                return false
            }

            if (!hasDrawn)
            {
                println("!!!!!!!!!!! HAS NOT DRAWN !!!!!!!!!!!")
                return false
            }

            when (cardNum)
            {
                in 0 until 5 ->
                {
                    val t = turnUIDmap[player_uid]?: return false
                    val discardable = hands[t]!!.cards[cardNum]

                    //TODO: If drawncard = null this means we're at the endgame, so we replace the drawn card with a dummy one?
                    hands[t]!!.cards[cardNum] = drawnCard
                    drawnCard = null

                    //cardDeck.add(discardable)
                    addCardToMarket(discardable)


                    gameINFO = "Waiting for Player ${currentTurn+1} to end their turn."
                    hasDiscarded = true
                    return true
                }
                else ->
                {
                    //cardDeck.add(drawnCard)
                    addCardToMarket(drawnCard!!)

                    drawnCard = null
                    gameINFO = "Waiting for Player ${currentTurn+1} to end their turn."
                    hasDiscarded = true
                    return true
                }
            }
        }
        return false
    }

    var drawnCard: Card? = null

    fun drawCard(player_uid: Long): Card?
    {
        if (lawSuitBlocker())
            return null

        // Check if player may perform this action
        if (isPlayerTurn(player_uid))
        {
            if (cardDeck.isEmpty())
            {
                // Do something here...
                hasDrawn = true
                gameINFO = "Waiting for Player ${currentTurn+1} to discard"
                return null
            }

            if (hasDrawn)
                return drawnCard

            if (drawnCard != null)
                return drawnCard

            val c = drawCard()
            drawnCard = c
            hasDrawn = true
            gameINFO = "Waiting for Player ${currentTurn+1} to discard"
            return c
        }
        else
        {
            return null
        }
    }

    fun playerLeft(player_uid: Long)
    {

    }

    fun anyMovesLeft(): Boolean
    {
        var foundAMove = false
        hands.map { it!!.cards.map { if (it != null) foundAMove = true } }
        return foundAMove
    }

    // TODO: Send the amount of cards left in the deck
    // TODO: Courtcase win is decided by unanimous coins.

    // Checks if a player can have a turn this is ONLY if their hand contains more than 1 card!
    fun turnIncrementor()
    {
        currentTurn++

        if (currentTurn > 3)
            currentTurn = 0

        if (cardDeck.isEmpty())
        {
            var nonNull = false
            hands[currentTurn]!!.cards.map { if (it != null) nonNull = true }

            if (!nonNull)
            {
                if (anyMovesLeft())
                    turnIncrementor()
                else
                    triggerEnd()
            }
        }
    }

    fun triggerEnd()
    {
        state = 2
    }

    fun emptyHandDiscard()
    {
        var nonNull = false
        hands[currentTurn]!!.cards.map { if (it != null) nonNull = true }

        if (!nonNull)
        {
            // player can't discard anything end fake discard.
            hasDiscarded = true
        }
    }

    fun endTurn(player_uid: Long): Boolean
    {
        if (lawSuitBlocker())
            return false

        return if (isPlayerTurn(player_uid))
        {
            if (hasDiscarded && hasDrawn)
            {
                gameINFO = "Player ${currentTurn} ended their turn"

                // TODO: Skip over players that can no longer have a turn.
                turnIncrementor()

                hasDiscarded = false

                hasDrawn = cardDeck.isEmpty()

                gameINFO = if (hasDrawn)
                    "Waiting for Player ${currentTurn+1} to discard"
                else
                    "Waiting for Player ${currentTurn+1} to draw."
                true
            }
            else
                false
        }
        else
        {
            false
        }
    }

    fun getSuits(arr: ArrayList<Card?>): ArrayList<Suits>
    {
        val list = ArrayList<Suits>()
        for (cards in arr)
        {
            if (cards != null)
                list.add(cards.suit)
        }

        return list
    }


    // TODO: After get objectives and there is no cards in the deck don't attempt to redraw!
    // TODO: Return null on empty deck
    fun drawCard(): Card?
    {
        if (cardDeck.isEmpty())
            return null

        /*
        if (cardDeck.size == 1)
        {
            val c = cardDeck.random()
            cardDeck.remove(c)
            reshuffleMarketIntoDeck()
            return c
        }*/

        val c = cardDeck.random()
        cardDeck.remove(c)
        return c
    }

    fun reshuffleMarketIntoDeck()
    {
        // We keep the last 2 cards out of the market
        val last_card_from_market = market.last()
        val second_last_card_from_market = market[market.lastIndex-1]

        market.remove(last_card_from_market)
        market.remove(second_last_card_from_market)

        cardDeck.addAll(market)
        cardDeck = cardDeck.shuffled()

        market = ArrayList()
        addCardToMarket(second_last_card_from_market)
        addCardToMarket(last_card_from_market)
    }

    fun handleGottonObjective(uid: Long)
    {
        // empty hand
        val hand = hands[turnUIDmap[uid]!!]
        hand!!.cards.map { if (it != null) addCardToMarket(it) }

        hands[turnUIDmap[uid]!!]!!.cards.removeAll(hand.cards)

        for (c in 0 until 5)
        {
                hand.cards.add(drawCard())
        }

        // Award Coins
        players[turnUIDmap[uid]!!]!!.coins++
    }

    fun attemptToGetObj(num: Int, uid: Long): Boolean
    {
        if (lawSuitBlocker())
            return false

        if (!isPlayerTurn(uid))
            return false

        // get player hand
        val hand = hands[turnUIDmap[uid]!!]?: return false

        // Check if hand contains all the cards that are in the objective
        for (arr in objectives.values)
        {
            val cpy_hand = getSuits(hand.cards)
            cpy_hand.removeAll(objectives[num]!!)
            if (cpy_hand.size <= 2)
            {
                handleGottonObjective(uid)
                setObj(num)
                emptyHandDiscard()
                return true
            }
        }
        emptyHandDiscard()
        return false
    }

    fun createLawsDeck()
    {
        var suits = Suits.FISH.baseArray()
        for (s in suits)
        {
            for (u in suits)
            {
                lawsDeck.add(YOUR_LAW(s, u))
            }
        }
    }

    fun latestLawSuitStatus(): LAWSUIT
    {
        var effect: Effects? = null

        if (market.size >= 2)
           effect = market.last().effect

        return LAWSUIT(lawSuitState, effect, plaintiffName, defendantName, courtsuitInfo(), plaintiffStake.get(), defendantStake.get())
    }

    fun courtsuitInfo(): String
    {
        return when (lawSuitState)
        {
            in  0..1 -> "Plaintiff accuses Defendant of being in Violation of discarding X after Y. Awaiting Defendant Response."
            2 -> "Spectators may bet on which party will win the case."
            3 -> "Awaiting Plaintiff to Press Charges or Withdraw the case"
            6 -> "The defendant was found... GUILTY"
            4 -> "The defendant was found... INNOCENT"
            5 -> "The defendant was found... INNOCENT"
            else -> "Not Implemented"
        }
    }

    fun latestGameState(): String
    {
        return gameINFO
    }

    fun getNames(uid: Long): PLAYER_LIST
    {
        val pl = PLAYER_LIST(null, null, null, null, -1)
        if (players[0] != null)
            pl.player1 = PLAYER_INFO(players[0]!!.name, players[0]!!.coins)
        if (players[1] != null)
            pl.player2 = PLAYER_INFO(players[1]!!.name, players[1]!!.coins)
        if (players[2] != null)
            pl.player3 = PLAYER_INFO(players[2]!!.name, players[2]!!.coins)
        if (players[3] != null)
            pl.player4 = PLAYER_INFO(players[3]!!.name, players[3]!!.coins)

        val iAM = turnUIDmap[uid]?: return pl
        pl.IAM = iAM

        return pl
    }

    fun getGamePrecedents(): ArrayList<Precedent>
    {
        return precedents.list
    }

    var defendantLost = true

    fun protection()
    {
        if (defendantLost)
        {
            val d = players[turnUIDmap[defendantUID]!!] ?: return
            d.coins++
        }
    }

    fun creditor()
    {
        val d = players[turnUIDmap[defendantUID]!!] ?: return
        d.coins++
    }

    fun guilty()
    {
        val d = players[turnUIDmap[defendantUID]!!] ?: return
        d.coins--
    }

    fun hearsay()
    {
        if (!lawVerified)
        {
            discardAndDrawLawCard(plaintiffUID)
        }
    }

    fun ministry()
    {
        if (!defendantLost)
        {
            discardAndDrawLawCard(defendantUID)
        }
    }

    fun transparency()
    {
        generateObjectives()
    }

    fun counsel()
    {
        redrawHand(plaintiffUID)
    }

    fun redrawHand(uid: Long)
    {
        if (cardDeck.isEmpty())
            return

        val player = turnUIDmap[uid]!!
        val newHand = ArrayList<Card?>()
        for (c in 0..4)
        {
            val card = cardDeck.random()
            cardDeck.remove(card)
            newHand.add(card)
        }
        val recycle = hands[player]!!.cards
        hands[player]!!.cards = newHand
        for (c in recycle)
        {
            cardDeck.add(c)
        }
    }

    fun getGAMEINFO(): GAME_INFO
    {
        return GAME_INFO(cardDeck.size, opacity)
    }
}

enum class Effects(val EffectName: String, val effectID: Int, val explination: String)
{
    PROTECTION("Protection", 0, "The defendant's costs for entering a lawsuit are covered by the state."),
    CREDITOR("Creditor", 1, "The defendant gets an extra coin."),
    MINISTRY("Ministry", 2, "After winning the court case get a law card."),
    EXEMPTION("Exemption", 3, "Can't get sued on this card"),
    COUNSEL("Counsel", 4, "The plaintiff's cards get exchanged for 5 new ones"),
    GUILTY("Guilty", 5, "The defendant loses 1 extra coin"),
    HEARSAY("Hearsay", 6, "The plaintiff's law card gets redrawn if it is not revealed"),
    FELONY("Felony", 7, "The card also has the resource of the card preceding it."),
    TRANSPARENCY("Transparency", 8, "The objectives will be rerolled after the court case."),
    BANKRUPTCY("Bankruptcy", 9, "Betting players lose their bet if they are wrong."),
    NONE("None", -1, "");

    fun resolve()
    {
        when (this)
        {
            CREDITOR -> PurgeryGame.creditor()
            GUILTY -> PurgeryGame.guilty()
            HEARSAY -> PurgeryGame.hearsay()
            TRANSPARENCY -> PurgeryGame.transparency()
            MINISTRY -> PurgeryGame.ministry()
            PROTECTION -> PurgeryGame.protection()
            COUNSEL -> PurgeryGame.counsel()
            else -> println("No effect!")
        }

    }

    fun next(): Effects
    {
        return when (this)
        {
            PROTECTION -> CREDITOR
            CREDITOR -> MINISTRY
            MINISTRY -> EXEMPTION
            EXEMPTION -> COUNSEL
            COUNSEL -> GUILTY
            GUILTY -> HEARSAY
            HEARSAY -> FELONY
            FELONY -> TRANSPARENCY
            TRANSPARENCY -> BANKRUPTCY
            BANKRUPTCY -> PROTECTION
            else -> NONE
        }
    }

    fun get(num: Int): Effects
    {
        return when (num)
        {
            0 -> PROTECTION
            1 -> CREDITOR
            2 -> MINISTRY
            3 -> EXEMPTION
            4 -> COUNSEL
            5 -> GUILTY
            6 -> HEARSAY
            7 -> FELONY
            8 -> TRANSPARENCY
            9 -> BANKRUPTCY
            else -> NONE
        }
    }
}

data class Player(var hand: Hand, val uid: Long, var name: String, var coins: Int = 3)
data class Hand(var cards: ArrayList<Card?>, var lawCard: YOUR_LAW?)
data class Card(var suit: Suits, val uid: Int, var effect: Effects)
enum class Suits(val suitName: String, var num: Int = 1)
{
    FISH("Fish"), FISH2("Fish", 2), FISH3("Fish", 3),
    HARVEST("Harvest"), HARVEST2("Harvest", 2), HARVEST3("Harvest", 3),
    BAKING("Baking"), BAKING2("Baking", 2), BAKING3("Baking", 3),
    LIVESTOCK("Livestock"), LIVESTOCK2("Livestock", 2), LIVESTOCK3("Livestock", 3),
    CONSTRUCTION("Construction"), CONSTRUCTION2("Construction", 2), CONSTRUCTION3("Construction", 3);

    fun baseArray():ArrayList<Suits>
    {
        val res = ArrayList<Suits>()
        res.addAll(arrayOf(FISH, HARVEST, LIVESTOCK, CONSTRUCTION, BAKING))
        return res
    }

    fun fullArray(): ArrayList<Suits>
    {
        val res = ArrayList<Suits>()
        res.addAll(arrayOf(FISH, HARVEST, BAKING, LIVESTOCK, CONSTRUCTION, FISH2, HARVEST2, BAKING2, LIVESTOCK2, CONSTRUCTION2, FISH3, HARVEST3, BAKING3, LIVESTOCK3, CONSTRUCTION3))
        return res
    }

    fun Random():Suits
    {
        return fullArray().random()
    }

    override fun toString(): String
    {
        return "$suitName [$num]"
    }
}
data class ObjectiveCard(var cardCombination: ArrayList<Card>)
data class PlayerAction(var p: Player, var action: String)

// SERVER DATA OBJECT SENDING
data class SERVER_DATASENDING_OBJECT(var action: SERVER_ACTION, val gameInfo: GAME_INFO? = null, var precedents: ArrayList<Precedent>? = null, var ls: LAWSUIT? = null, var objectives: HashMap<Int, Array<Suits>>? = null,
                                     var currentTurn: Int? = null, var currentHand: Hand? = null, var playerList: PLAYER_LIST? = null,
                                     var drawnCard: Card? =null, var ping: String? = null, var info: String? = null, var uid: Long? = null,
                                     var discardPile: ArrayList<Card>? = null)

data class PLAYER_LIST(var player1: PLAYER_INFO?, var player2: PLAYER_INFO?, var player3: PLAYER_INFO?, var player4: PLAYER_INFO?, var IAM: Int = -1)
data class PLAYER_INFO(var name: String? = null, var coins: Int? = null)
enum class SERVER_ACTION{REJECT, INFO}
data class YOUR_LAW(var suit1: Suits, var suit2: Suits)
data class LAWSUIT(var state: Int = -1, var effect: Effects?, var plaintiffName: String = "", var defendantName: String = "",  var courtInfoText: String = "", val plaintiffBets: Int = 0, val defendantBets: Int = 0, var law: YOUR_LAW? = null)
data class PRECEDENTS(var list: ArrayList<Precedent>)
data class Precedent(var accusee: String, var accused: String, var suit1: Suits, var suit2: Suits, var verdict: String)
data class GAME_INFO(var remainingCards: Int, var opacity: Double = 1.0)

// Server RECV Objects
data class SERVER_RECV_OBJECT(var action: SRO_ACTION, var uid: Long? = null, var userName: String, var info: String? = null)
enum class SRO_ACTION{JOIN, DRAW, DISCARD1, DISCARD2, DISCARD3, DISCARD4, DISCARD5, DISCARD6, OBJECTIVES, SUE1, SUE2, SUE3, SUE4, PLAYERS, INFO, ENDTURN, INIT, OBJ1, OBJ2, OBJ3, GUILTY, FIGHT, PWIN, DWIN, ABSTAIN, WITHDRAW, PRESS}