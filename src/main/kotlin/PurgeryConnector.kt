import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

/**
 *
 */
object PurgeryConnector
{
    val bc = BasicClient()
    var connected = false

    var DRAWN_CARD: Card? = null

    /**
     * Attempts to connect to the client
     */
    fun connect(username: String)
    {
        bc.username = username
        bc.commandParser("/join")
    }

    fun synchroThread()
    {
        GlobalScope.launch {
            delay(5000L)
            while (true)
            {
                delay(500)
                if (connected)
                    info()
            }
        }
    }

    fun courtOption(n: Int)
    {
        when (n)
        {
            0 -> bc.commandParser("/guilty")
            1 -> bc.commandParser("/fight")
            2 -> bc.commandParser("/pwin")
            3 -> bc.commandParser("/dwin")
            4 -> bc.commandParser("/abstain")
            5 -> bc.commandParser("/withdraw")
            6 -> bc.commandParser("/press")
        }
    }

    fun sue(n: Int)
    {
        when (n)
        {
            1 -> bc.commandParser("/s1")
            2 -> bc.commandParser("/s2")
            3 -> bc.commandParser("/s3")
            4 -> bc.commandParser("/s4")
        }
    }

    fun init()
    {
        bc.commandParser("/init")
    }


    fun discard(n: Int)
    {
        DRAWN_CARD = null
        when (n)
        {
            1 -> bc.commandParser("/d1")
            2 -> bc.commandParser("/d2")
            3 -> bc.commandParser("/d3")
            4 -> bc.commandParser("/d4")
            5 -> bc.commandParser("/d5")
            6 -> bc.commandParser("/d6")
        }
    }

    fun objective(n: Int)
    {
        when (n)
        {
            1 -> bc.commandParser("/o1")
            2 -> bc.commandParser("/o2")
            3 -> bc.commandParser("/o3")
        }
    }

    fun info()
    {
        bc.commandParser("/nfo")
    }

    fun endTurn()
    {
        bc.commandParser("/turn")
    }

    fun drawCard()
    {
        bc.commandParser("/draw")

    }

    /**
     * Whenever the server answers we get the result here.
     */
    fun onServerAnswerSuccess(sdo: SERVER_DATASENDING_OBJECT)
    {
        if (sdo.drawnCard != null)
        {
            DRAWN_CARD = sdo.drawnCard
        }

        if (DRAWN_CARD != null)
            sdo.drawnCard = DRAWN_CARD

        Display.controller!!.setLabels(sdo)
        Display.controller!!.connectSuccess()
    }

    fun onServerAnswerRequestIgnored(sdo: SERVER_DATASENDING_OBJECT)
    {
        if (sdo.info == null)
        {
            connected = false
            Display.controller!!.connectFail()
        }
        else
        {
            Display.controller!!.infoFromServer(sdo.info!!)
        }
    }
}