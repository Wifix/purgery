import javafx.animation.Animation
import javafx.animation.PauseTransition
import javafx.application.Application
import javafx.application.Platform
import javafx.event.ActionEvent
import javafx.event.EventHandler
import javafx.fxml.FXML
import javafx.fxml.FXMLLoader
import javafx.fxml.Initializable
import javafx.scene.Parent
import javafx.scene.Scene
import javafx.scene.control.Button
import javafx.scene.control.ListView
import javafx.scene.control.TextField
import javafx.scene.image.Image
import javafx.scene.image.ImageView
import javafx.scene.input.MouseButton
import javafx.scene.input.MouseEvent
import javafx.scene.layout.AnchorPane
import javafx.scene.media.AudioClip
import javafx.scene.media.Media
import javafx.scene.media.MediaPlayer
import javafx.scene.paint.Color
import javafx.scene.paint.Paint
import javafx.scene.shape.Rectangle
import javafx.scene.text.Font
import javafx.scene.text.Text
import javafx.scene.text.TextAlignment
import javafx.stage.Stage
import javafx.util.Duration
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import mu.KotlinLogging
import java.io.File
import java.lang.Exception
import java.net.URL
import java.util.*
import kotlin.math.ln
import kotlin.math.round

private val log = KotlinLogging.logger {}

/**
 *
 */
class Display : Application()
{
    companion object
    {
        var stage: Stage? = null
        var controller: DisplayController? = null

    }

    override fun start(primaryStage: Stage?)
    {
        val fxmlLoader = FXMLLoader()
        val root = fxmlLoader.load<Parent>(javaClass.getResource("/vis.fxml").openStream())
        controller = fxmlLoader.getController()
        val scene = Scene(root, 1280.0, 900.0)
        primaryStage!!.scene = scene
        stage = primaryStage
        primaryStage.show()
    }
}

class DisplayController : Initializable
{
    val eh = EffectHover().get()
    var handcount = 0

    override fun initialize(location: URL?, resources: ResourceBundle?)
    {
        anchor_login.isVisible = true
        username.isVisible = false
        connectToGame.isVisible = false
        GlobalScope.launch(Dispatchers.IO) {
            try
            {
                PurgeryConnector.bc.connect()
                println("Client Done")
                PurgeryConnector.synchroThread()
                Platform.runLater {
                    username.isVisible = true
                    connectToGame.isVisible = true
                }
            }
            catch (ex: Exception)
            {
                serverOffline()
            }
        }
        plaintiffCoins = arrayListOf(plaintiffCoin_1, plaintiffCoin_2, plaintiffCoin_3, plaintiffCoin_4, plaintiffCoin_5)
        defendantCoins = arrayListOf(defendantCoin_1, defendantCoin_2, defendantCoin_3, defendantCoin_4, defendantCoin_5)
        createDiscardPile()
        playBGM()
        game.children.add(eh)
        eh.toFront()
        eh.isVisible = false

        animation = PauseTransition(Duration.seconds(1.0))
        animation.onFinished = EventHandler {
            // TODO
            /*
            when (handcount)
            {

            }*/
            /*
            txt_effect.text = discPile[c].effect.explination
            txt.text = discPile[c].effect.EffectName
            popup_effect.isVisible = true
            popup_effect.toFront()
            popup_effect.layoutX = imgv.x
            popup_effect.layoutY = imgv.y + 15.0*/
        }
        populateList()
    }

    fun showDescBox(txt_effect: Text, imgv: ImageView, effects: Effects)
    {
        txt_effect.text = effects.explination
        popup_effect.isVisible = true
        popup_effect.toFront()
        popup_effect.layoutX = imgv.x
        popup_effect.layoutY = imgv.y + 15.0
    }

    fun connect(actionEvent: ActionEvent)
    {
        // TODO: Sanity check
        if (username.text.length in 3..12)
        {
            Platform.runLater {
                anchor_login.isVisible = false
                anchor_connecting.isVisible = true
            }

            GlobalScope.launch {
                PurgeryConnector.connect(username.text)
            }
        }
    }

    fun connectSuccess()
    {
        Platform.runLater {
            anchor_connecting.isVisible = false
            game.isVisible = true
        }

    }

    fun connectFail()
    {
        Platform.runLater {
            anchor_connecting.isVisible = false
            anchor_login.isVisible = true
        }
    }

    fun drawCard(mouseEvent: MouseEvent)
    {
        PurgeryConnector.drawCard()
    }

    fun clearFill()
    {
        p1rect.fill = Color.DODGERBLUE
        p2rect.fill = Color.DODGERBLUE
        p3rect.fill = Color.DODGERBLUE
        p4rect.fill = Color.DODGERBLUE
        player1TurnIndicator.isVisible = false
        player2TurnIndicator.isVisible = false
        player3TurnIndicator.isVisible = false
        player4TurnIndicator.isVisible = false
    }

    fun getObj1()
    {
        PurgeryConnector.objective(1)
    }

    fun getObj2()
    {
        PurgeryConnector.objective(2)
    }

    fun getObj3()
    {
        PurgeryConnector.objective(3)
    }

    fun getXCoordinateForMarket(): Double
    {
        return when(currentDiscardSize)
        {
            in 0..7 ->  60.0 + (155.0 * currentDiscardSize)
            in 8..15 -> 60.0 + (155.0 * (currentDiscardSize-8))
            in 16..23 -> 60.0 + (155.0 * (currentDiscardSize-16))
            else -> 0.0
        }
    }

    fun getYCoordinateForMarket(): Double
    {
        return when(currentDiscardSize)
        {
            in 0..7 ->  60.0
            in 8..15 -> 265.0
            in 16..23 -> 410.0
            else -> 0.0
        }
    }

    var cachedTurn: Int? = 0
    var currentDiscardSize = 0
    var marketImageViews = ArrayList<ImageView>()
    var activeSuit = false

    @FXML
    private lateinit var courtRoom: AnchorPane

    @FXML
    private lateinit var bWithdraw: Button

    @FXML
    private lateinit var bPressCharges: Button

    @FXML
    private lateinit var bGuilty: Button

    @FXML
    private lateinit var bFight: Button

    @FXML
    private lateinit var bPlaintiffWin: Button

    @FXML
    private lateinit var bDefendantWin: Button

    @FXML
    private lateinit var bAbstain: Button

    @FXML
    private lateinit var txt_courtOverview: Text

    fun pleadGuiltyButton()
    {
        PurgeryConnector.courtOption(0)
    }



    fun handleLawSuitState(n: Int)
    {
        activeSuit = true
        Platform.runLater {
            courtRoom.toFront()
            courtRoom.isVisible = true
        }

        when (n)
        {
            0 -> state0()
            1 -> state1()
            2 -> state2()
            3 -> state3()
            4 -> state4()
            6 -> state6()
            5 -> state5()
            else -> allHidden()
        }

    }

    fun allHidden()
    {
        Platform.runLater {
            bWithdraw.isVisible = false
            bPressCharges.isVisible = false
            bGuilty.isVisible = false
            bFight.isVisible = false
            bPlaintiffWin.isVisible = false
            bDefendantWin.isVisible = false
            bAbstain.isVisible = false

            img_perjury.isVisible = false
            imgGuilty.isVisible = false
            imgNotGuilty.isVisible = false
        }
    }

    var faderRunning = false
    fun courtRoomFader(duration: Long)
    {
        if (faderRunning) return

        faderRunning = true

        var opacity = 1.0
        var mils: Long = 0
        var rate = 100L
        val steps = (duration / (rate / 1000.0))
        val increment = opacity / steps
        var loopcount = 0

        GlobalScope.launch {
            while (mils <= duration*1000)
            {
                val lnResult = -1 * ln(loopcount * increment)
                opacity = if (lnResult >= 1.0) 1.0 else if (lnResult <= 0) 0.0 else lnResult

                Platform.runLater { courtRoom.opacity = opacity }

                delay(rate)
                mils += 100
                loopcount++
            }
            opacity = 0.0
        }
    }


    fun verdictStater()
    {
        courtRoomFader(10)

        if (!verdictGavel)
        {
            playGavelSound()
            verdictGavel = true
        }

        allHidden()
    }

    fun state5()
    {
        verdictStater()

        Platform.runLater {
            img_perjury.isVisible = true
            imgNotGuilty.isVisible = true
        }
    }

    fun state6()
    {
        verdictStater()

        Platform.runLater { imgGuilty.isVisible = true }
    }

    fun state4()
    {
        verdictStater()

        Platform.runLater {
            imgNotGuilty.isVisible = true
        }
    }


    fun state3()
    {
        gavelplayed = false
        Platform.runLater {
            bWithdraw.isVisible = true
            bPressCharges.isVisible = true
            bGuilty.isVisible = false
            bFight.isVisible = false
            bPlaintiffWin.isVisible = false
            bDefendantWin.isVisible = false
            bAbstain.isVisible = false
        }
    }

    fun state2()
    {
        gavelplayed = false
        Platform.runLater {
            bWithdraw.isVisible = false
            bPressCharges.isVisible = false
            bGuilty.isVisible = false
            bFight.isVisible = false
            bPlaintiffWin.isVisible = true
            bDefendantWin.isVisible = true
            bAbstain.isVisible = true
        }
    }

    fun state1()
    {
        Platform.runLater {
            bWithdraw.isVisible = false
            bPressCharges.isVisible = false
            bGuilty.isVisible = true
            bFight.isVisible = true
            bPlaintiffWin.isVisible = false
            bDefendantWin.isVisible = false
            bAbstain.isVisible = false
        }
    }

    var gavelplayed = false
    var verdictGavel = false
    fun state0()
    {
        verdictGavel = false
        if (!gavelplayed)
        {
            playGavelSound()
            gavelplayed = true
        }
        faderRunning = false
        allHidden()
        state1()
    }

    fun populateList()
    {
        GlobalScope.launch {
            Platform.runLater {
                historylist.style = "-fx-background: #1e1e1e; -fx-control-inner-background: #515658;"
                historylist.items.clear()
            }

            val pList = cachedPrecedents.list.reversed()
            for (p in pList)
            {
                val pr = PrecedentMaker().createPrecedent(p)
                Platform.runLater {
                    historylist.items.add(pr)
                }
            }
        }
    }

    var overPButton = false
    var overPPane = false

    fun hidePrecedent(mouseEvent: MouseEvent)
    {
        if ((mouseEvent.source as AnchorPane).id == "anchorHistory")
            overPButton = false
        else if ((mouseEvent.source as AnchorPane).id == "anchorHistoryList")
            overPPane= false

        Platform.runLater{
            if (!overPButton && !overPPane)
                anchorHistoryList.isVisible = false
        }
    }

    fun showPrecedent(mouseEvent: MouseEvent)
    {

        if ((mouseEvent.source as AnchorPane).id == "anchorHistory")
            overPButton = true
        else if ((mouseEvent.source as AnchorPane).id == "anchorHistoryList")
            overPPane= true

        Platform.runLater {
            anchorHistoryList.toFront()
            if (overPButton || overPPane)
                anchorHistoryList.isVisible = true
        }
    }

    @FXML
    private lateinit var anchorHistoryList: AnchorPane

    @FXML
    private lateinit var anchorHistory: AnchorPane

    @FXML
    private lateinit var historylist: ListView<AnchorPane>

    private lateinit var animation: Animation

    fun showCardEffect(txt: Text, x: Double, y: Double, effect: Effects)
    {
        txt_effect.text = effect.explination
        popup_effect.isVisible = true
        popup_effect.toFront()
        popup_effect.layoutX = x
        popup_effect.layoutY = y
    }

    fun hideCardEffect()
    {
        Platform.runLater {
            popup_effect.isVisible = false
        }
    }

    fun cardModEnter(mouseEvent: MouseEvent)
    {
        val ch = cachedHand ?: return
        val txt = mouseEvent.source as Text

        //TODO send card?
        when (txt.id)
        {
            "txt_card_mod_1" -> showCardEffect(txt, 200.0, 585.0, ch.cards[0]!!.effect)
            "txt_card_mod_2" -> showCardEffect(txt, 350.0, 585.0, ch.cards[1]!!.effect)
            "txt_card_mod_3" -> showCardEffect(txt, 500.0, 585.0, ch.cards[2]!!.effect)
            "txt_card_mod_4" -> showCardEffect(txt, 650.0, 585.0, ch.cards[3]!!.effect)
            "txt_card_mod_5" -> showCardEffect(txt, 800.0, 585.0, ch.cards[4]!!.effect)
            "txt_card_mod_6" -> showCardEffect(txt, 950.0, 585.0, cachedDraw!!.effect)
            else -> println(txt.id)
        }
    }

    fun cardModExit(mouseEvent: MouseEvent)
    {
        hideCardEffect()
    }

    val discardPile = ArrayList<ImageView>()
    val discardText = ArrayList<Text>()

    fun createDiscardPile()
    {
        for (c in 0..43)
        {
            val imgv = ImageView()
            imgv.fitWidth = 104.0
            imgv.fitHeight = 139.0
            imgv.isVisible = false
            imgv.x = calcXCoordDiscardPile(c)
            imgv.y = calcYCoordDiscardPile(c)


            val txt = Text()
            val delay = PauseTransition(Duration.seconds(1.0)) as Animation
            delay.onFinished = EventHandler {
                txt_effect.text = discPile[c].effect.explination
                txt.text = discPile[c].effect.EffectName
                popup_effect.isVisible = true
                popup_effect.toFront()
                popup_effect.layoutX = imgv.x
                popup_effect.layoutY = imgv.y + 15.0
            }

            txt.onMouseEntered = EventHandler {
                delay.playFromStart()
            }

            txt.onMouseExited = EventHandler {
                popup_effect.isVisible = false
                delay.stop()
            }

            val f = Font(16.0)
            txt.isVisible = false
            txt.wrappingWidth = 104.0
            txt.font = f
            txt.fill = Paint.valueOf("#206cdb")
            txt.textAlignment = TextAlignment.CENTER
            txt.x = imgv.x
            txt.y = imgv.y + 12.5

            game.children.add(imgv)
            game.children.add(txt)

            discardPile.add(imgv)
            discardText.add(txt)
        }
    }


    @FXML
    private lateinit var popup_effect: AnchorPane

    @FXML
    private lateinit var txt_effect: Text

    fun calcXCoordDiscardPile(n: Int): Double
    {
        return when(n)
        {
            in 0..10 ->  55.0 + (110.0 * n)
            in 11..21 -> 55.0 + (110.0 * (n-11))
            in 22..32 -> 55.0 + (110.0 * (n-22))
            else -> 55.0 + (110.0 * (n-33))
        }
    }

    fun calcYCoordDiscardPile(n: Int): Double
    {
        return when(n)
        {
            in 0..10 ->  55.0
            in 11..21 -> 200.0
            in 22..32 -> 345.0
            else -> 490.0
        }
    }

    fun suitToImage(suit: Suits): Image
    {
        return Image("/cards/${suit.suitName.toLowerCase()}x${suit.num}.png")
    }

    fun playGavelSound()
    {
        println("PLAY THIS SHIT")
        Platform.runLater {
            val media = Media(DisplayController::class.java.getResource("/gavel.mp3").toURI().toString())
            val ac = AudioClip(media.source)
            ac.volume = 0.03
            ac.cycleCount = 1
            ac.play()
        }
    }

    fun playBGM()
    {
        Platform.runLater {
            val media = Media(DisplayController::class.java.getResource("/sjkrrmBnpGE.mp3").toURI().toString())
            val ac = AudioClip(media.source)
            ac.volume = 0.03
            ac.cycleCount = AudioClip.INDEFINITE
            ac.play()
        }
    }

    @FXML
    private lateinit var servernotonline: Text

    @FXML
    private lateinit var connectToGame: Button

    fun serverOffline()
    {
        Platform.runLater {
            servernotonline.isVisible = true
            username.isVisible = false
            connectToGame.isVisible = false
            anchor_connecting.isVisible = false
            anchor_login.isVisible = true
        }
    }

    fun playJoin()
    {
        Platform.runLater {
            val media = Media(DisplayController::class.java.getResource("/join.mp3").toURI().toString())
            val ac = AudioClip(media.source)
            ac.volume = 0.05
            ac.play()
        }
    }

    fun playBell()
    {
        Platform.runLater {
            val media = Media(DisplayController::class.java.getResource("/ding.mp3").toURI().toString())
            val ac = AudioClip(media.source)
            ac.volume = 0.05
            ac.play()
        }
    }

    fun outOfDate()
    {
        Platform.runLater {
            connecting.text = "The version of this Client does not support this server version"
        }
    }

    @FXML
    private lateinit var plaintiffLaw1: ImageView

    @FXML
    private lateinit var plaintiffLaw2: ImageView

    @FXML
    private lateinit var defendantDiscard1: ImageView

    @FXML
    private lateinit var defendantDiscard2: ImageView

    var discPile = ArrayList<Card>()
    var cachedPrecedents = PRECEDENTS(ArrayList())
    var cachedHand: Hand? = null
    var cachedDraw: Card? = null

    fun handCardImageHandler(card: Card?, imgv: ImageView)
    {
        if (card == null)
        {
            Platform.runLater { imgv.isVisible = false }
        }
        else
        {
            Platform.runLater{imgv.image = suitToImage(card.suit)}
        }
    }

    fun handleCardModText(card: Card?, txt: Text)
    {
        if (card == null)
        {
            Platform.runLater { txt.isVisible = false }
        }
        else
        {
            Platform.runLater { txt.text = card.effect.EffectName }
        }
    }

    // TODO: Move from UI Thread to GlobalScope
    fun setLabels(sdo: SERVER_DATASENDING_OBJECT)
    {
        Platform.runLater {
            if (sdo.precedents != null)
            {
                if (cachedPrecedents.list != sdo.precedents)
                {
                    cachedPrecedents = PRECEDENTS(sdo.precedents!!)
                    populateList()
                }

            }
            if (sdo.ls != null)
            {
                val ls = sdo.ls ?: return@runLater

                when (ls.effect?.explination)
                {
                    null -> { }
                    else -> Platform.runLater {
                            txt_courtOverview.text = ls.courtInfoText
                            plaintiff_name.text = ls.plaintiffName
                            defendant_name.text = ls.defendantName
                            txt_effectOverview.text = ls.effect!!.explination
                        }
                }

                if (dCoins != ls.defendantBets)
                {
                    displayCoins(ls.defendantBets, defendantCoins)
                    dCoins = ls.defendantBets
                }

                if (pCoins != ls.plaintiffBets)
                {
                    displayCoins(ls.plaintiffBets, plaintiffCoins)
                    pCoins = ls.plaintiffBets
                }

                when (val state = ls.state)
                {
                    in 0..6 -> handleLawSuitState(state)
                    else ->
                    {
                        activeSuit = false
                        Platform.runLater { courtRoom.isVisible = false }
                    }
                }

                if (ls.law != null)
                {
                    val c1 = ls.law!!.suit1
                    val c2 = ls.law!!.suit2

                    val dc1 = sdo.discardPile!![sdo.discardPile!!.lastIndex-1]
                    val dc2 = sdo.discardPile!!.last()

                    Platform.runLater {
                        plaintiffLaw1.image = Image("/cards/${c1.suitName.toLowerCase()}x${c1.num}.png")
                        plaintiffLaw2.image = Image("/cards/${c2.suitName.toLowerCase()}x${c2.num}.png")
                        defendantDiscard1.image = Image("/cards/${dc1.suit.suitName.toLowerCase()}x${dc1.suit.num}.png")
                        defendantDiscard2.image = Image("/cards/${dc2.suit.suitName.toLowerCase()}x${dc2.suit.num}.png")
                    }
                }
                else if (ls.state != -1)
                {
                    val dc1 = sdo.discardPile!![sdo.discardPile!!.lastIndex-1]
                    val dc2 = sdo.discardPile!!.last()

                    Platform.runLater {
                        plaintiffLaw1.image = Image("/cards/${dc1.suit.suitName.toLowerCase()}x${dc1.suit.num}.png")
                        plaintiffLaw2.image = Image("/cards/${dc2.suit.suitName.toLowerCase()}x${dc2.suit.num}.png")
                        defendantDiscard1.image = Image("/cards/${dc1.suit.suitName.toLowerCase()}x${dc1.suit.num}.png")
                        defendantDiscard2.image = Image("/cards/${dc2.suit.suitName.toLowerCase()}x${dc2.suit.num}.png")
                    }
                }
            }
            if (sdo.discardPile != null)
            {
                when
                {
                    sdo.discardPile!!.size > currentDiscardSize ->
                    {
                        discPile = sdo.discardPile  ?: return@runLater
                        for (c in 0 until discPile.size)
                        {
                            Platform.runLater {
                                discardPile[c].image = suitToImage(discPile[c].suit)
                                discardPile[c].isVisible = true
                                discardText[c].text = discPile[c].effect.EffectName
                                discardText[c].isVisible = true
                            }
                        }
                        currentDiscardSize = sdo.discardPile!!.size
                    }
                    sdo.discardPile!!.size == currentDiscardSize ->
                    {}
                    else ->
                    {
                        for (imgv in discardPile)
                            Platform.runLater {
                                imgv.isVisible = false
                            }
                        for (txt in discardText)
                            Platform.runLater {
                                txt.isVisible = false
                            }
                        currentDiscardSize = 0
                    }
                }
            }
            if (sdo.objectives != null)
            {
                   Platform.runLater {
                       var suit = sdo.objectives!![2]!![0]
                       img_obj_3_1.image = Image("/cards/${suit.suitName.toLowerCase()}x${suit.num}.png")
                       suit = sdo.objectives!![2]!![1]
                       img_obj_3_2.image = Image("/cards/${suit.suitName.toLowerCase()}x${suit.num}.png")
                       suit = sdo.objectives!![2]!![2]
                       img_obj_3_3.image = Image("/cards/${suit.suitName.toLowerCase()}x${suit.num}.png")

                       suit = sdo.objectives!![1]!![0]
                       img_obj_2_1.image = Image("/cards/${suit.suitName.toLowerCase()}x${suit.num}.png")
                       suit = sdo.objectives!![1]!![1]
                       img_obj_2_2.image = Image("/cards/${suit.suitName.toLowerCase()}x${suit.num}.png")
                       suit = sdo.objectives!![1]!![2]
                       img_obj_2_3.image = Image("/cards/${suit.suitName.toLowerCase()}x${suit.num}.png")

                       suit = sdo.objectives!![0]!![0]
                       img_obj_1_1.image = Image("/cards/${suit.suitName.toLowerCase()}x${suit.num}.png")
                       suit = sdo.objectives!![0]!![1]
                       img_obj_1_2.image = Image("/cards/${suit.suitName.toLowerCase()}x${suit.num}.png")
                       suit = sdo.objectives!![0]!![2]
                       img_obj_1_3.image = Image("/cards/${suit.suitName.toLowerCase()}x${suit.num}.png")
                   }
            }
            if (sdo.currentTurn == I_AM_PLAYER)
            {
                if (!turn_indicator_played)
                {
                    turn_indicator_played = true
                    playBell()
                }
            }
            else
            {
                turn_indicator_played = false
            }
            if (sdo.currentTurn != cachedTurn)
            {
                clearFill()
                when (sdo.currentTurn)
                {
                    0 -> {p1rect.fill = Color.web("#1fe1ff"); player1TurnIndicator.isVisible = true }
                    1 -> {p2rect.fill = Color.web("#1fe1ff"); player2TurnIndicator.isVisible = true }
                    2 -> {p3rect.fill = Color.web("#1fe1ff"); player3TurnIndicator.isVisible = true }
                    3 -> {p4rect.fill = Color.web("#1fe1ff"); player4TurnIndicator.isVisible = true }
                }
                cachedTurn = sdo.currentTurn
            }
            if (sdo.currentHand != null)
            {
                cachedHand = sdo.currentHand

                lateinit var suit: Suits

                handCardImageHandler(sdo.currentHand!!.cards[0], img_card1)
                handCardImageHandler(sdo.currentHand!!.cards[1], img_card2)
                handCardImageHandler(sdo.currentHand!!.cards[2], img_card3)
                handCardImageHandler(sdo.currentHand!!.cards[3], img_card4)
                handCardImageHandler(sdo.currentHand!!.cards[4], img_card5)

                /*
                var suit = sdo.currentHand!!.cards[0].suit
                img_card1.image = Image("/cards/${suit.suitName.toLowerCase()}x${suit.num}.png")
                suit = sdo.currentHand!!.cards[1].suit
                img_card2.image = Image("/cards/${suit.suitName.toLowerCase()}x${suit.num}.png")
                suit = sdo.currentHand!!.cards[2].suit
                img_card3.image = Image("/cards/${suit.suitName.toLowerCase()}x${suit.num}.png")
                suit = sdo.currentHand!!.cards[3].suit
                img_card4.image = Image("/cards/${suit.suitName.toLowerCase()}x${suit.num}.png")
                suit = sdo.currentHand!!.cards[4].suit
                img_card5.image = Image("/cards/${suit.suitName.toLowerCase()}x${suit.num}.png")
                */
                val h = sdo.currentHand
                if (h != null)
                {
                    if (h.lawCard != null)
                    {
                        val lc = h.lawCard
                        suit = lc!!.suit1
                        img_law_1.image = Image("/cards/${suit.suitName.toLowerCase()}x${suit.num}.png")
                        suit = lc.suit2
                        img_law_2.image = Image("/cards/${suit.suitName.toLowerCase()}x${suit.num}.png")
                    }

                    handleCardModText(h.cards[0], txt_card_mod_1)
                    handleCardModText(h.cards[1], txt_card_mod_2)
                    handleCardModText(h.cards[2], txt_card_mod_3)
                    handleCardModText(h.cards[3], txt_card_mod_4)
                    handleCardModText(h.cards[4], txt_card_mod_5)

                    /*
                    txt_card_mod_1.text = h.cards[0].effect.EffectName
                    txt_card_mod_2.text = h.cards[1].effect.EffectName
                    txt_card_mod_3.text = h.cards[2].effect.EffectName
                    txt_card_mod_4.text = h.cards[3].effect.EffectName
                    txt_card_mod_5.text = h.cards[4].effect.EffectName
                    */
                }



            }
            if (sdo.drawnCard == null)
            {
                img_card6.isVisible = false
                hb_drawncard.isVisible = false
            }
            else
            {
                cachedDraw = sdo.drawnCard
                img_card6.isVisible = true
                var suit = sdo.drawnCard!!.suit
                img_card6.image = Image("/cards/${suit.suitName.toLowerCase()}x${suit.num}.png")

                txt_card_mod_6.text = sdo.drawnCard!!.effect.EffectName

                hb_drawncard.isVisible = true
            }
            if (sdo.info != null)
            {
                currentAction.text = sdo.info
            }
            if (sdo.playerList != null)
            {
                setPlayerListInfo(txt_p1name, sdo.playerList!!.player1)
                setPlayerListInfo(txt_p2name, sdo.playerList!!.player2)
                setPlayerListInfo(txt_p3name, sdo.playerList!!.player3)
                setPlayerListInfo(txt_p4name, sdo.playerList!!.player4)

                val iam = sdo.playerList!!.IAM
                if (iam != -1)
                {
                    I_AM_PLAYER = iam
                }
            }
            if (sdo.gameInfo != null)
            {
                handleGameInfo(sdo.gameInfo)
            }
        }

    }

    @FXML
    private lateinit var txt_remaining: Text

    var cachedRemaining = 50
    private fun handleGameInfo(gameInfo: GAME_INFO)
    {
        if (cachedRemaining != gameInfo.remainingCards)
        {
            cachedRemaining = gameInfo.remainingCards
            Platform.runLater {
                txt_remaining.text = "$cachedRemaining"
            }
        }
    }

    var turn_indicator_played = false
    var I_AM_PLAYER = -1

    fun setPlayerListInfo(text: Text, info: PLAYER_INFO?)
    {
        var playSoundOnChange = text.text == "?"

        if (info == null)
            text.text = "?"
        else
        {
            if (playSoundOnChange)
                playJoin()
            text.text = "[${info.coins}] ${info.name}"
        }

    }

    fun cardDiscard(mouseEvent: MouseEvent)
    {
        val src = mouseEvent.source as AnchorPane
        when (src.id)
        {
            "hb_card1" -> PurgeryConnector.discard(1)
            "hb_card2" -> PurgeryConnector.discard(2)
            "hb_card3" -> PurgeryConnector.discard(3)
            "hb_card4" -> PurgeryConnector.discard(4)
            "hb_card5" -> PurgeryConnector.discard(5)
            "hb_drawncard" -> PurgeryConnector.discard(6)
        }
    }

    fun infoFromServer(text: String)
    {
        Platform.runLater {
            currentAction.text = text
        }
    }

    fun endTurn(mouseEvent: MouseEvent)
    {
        PurgeryConnector.endTurn()
    }

    fun resetGame(mouseEvent: MouseEvent)
    {
        if (mouseEvent.button == MouseButton.MIDDLE)
        {
            PurgeryConnector.init()
        }
    }

    var overButton = false
    var overPane = false

    var overLButton = false
    var overLPane = false


    fun hideLaw(mouseEvent: MouseEvent)
    {
        if ((mouseEvent.source as AnchorPane).id == "anchorLaw")
            overLButton = false
        else if ((mouseEvent.source as AnchorPane).id == "anchorLaw2")
            overLPane= false

        Platform.runLater{
            if (!overLButton && !overLPane)
                anchorLaw2.isVisible = false
        }
    }

    fun showLaw(mouseEvent: MouseEvent)
    {
        if ((mouseEvent.source as AnchorPane).id == "anchorLaw")
            overLButton = true
        else if ((mouseEvent.source as AnchorPane).id == "anchorLaw2")
            overLPane= true

        Platform.runLater {
            anchorLaw2.toFront()
            if (overLButton || overLPane)
                anchorLaw2.isVisible = true
        }
    }


    fun hideObj(mouseEvent: MouseEvent)
    {
        if ((mouseEvent.source as AnchorPane).id == "anchorObjectiveAppear")
            overButton = false
        else if ((mouseEvent.source as AnchorPane).id == "anchorObjective")
            overPane= false

        Platform.runLater{
            if (!overButton && !overPane)
                anchorObjective.isVisible = false
        }
    }

    fun showObj(mouseEvent: MouseEvent)
    {
        if ((mouseEvent.source as AnchorPane).id == "anchorObjectiveAppear")
            overButton = true
        else if ((mouseEvent.source as AnchorPane).id == "anchorObjective")
            overPane= true

        Platform.runLater {
            anchorObjective.toFront()
            if (overButton || overPane)
                anchorObjective.isVisible = true
        }
    }

    fun suePlayer(mouseEvent: MouseEvent)
    {
        val src = mouseEvent.source as AnchorPane
        when (src.id)
        {
            "player1displayAnchor" -> sue(1)
            "player2displayAnchor" -> sue(2)
            "player3displayAnchor" -> sue(3)
            "player4displayAnchor" -> sue(4)
        }
    }

    fun sue(n: Int)
    {
        Platform.runLater { confirmLawSuitAnchor.isVisible = true }
        sueTarget = n
        when (n)
        {
            1 -> Platform.runLater { txt_targetSue.text = "Target: P1 ${txt_p1name.text}" }
            2 -> Platform.runLater { txt_targetSue.text = "Target: P2 ${txt_p2name.text}" }
            3 -> Platform.runLater { txt_targetSue.text = "Target: P3 ${txt_p3name.text}" }
            4 -> Platform.runLater { txt_targetSue.text = "Target: P4 ${txt_p4name.text}" }
        }
    }

    var sueTarget = 0

    fun sueYes()
    {
        PurgeryConnector.sue(sueTarget)
        sueTarget = 0
        Platform.runLater { confirmLawSuitAnchor.isVisible = false }
    }

    fun sueNo()
    {
        sueTarget = 0
        Platform.runLater { confirmLawSuitAnchor.isVisible = false }
    }

    fun fightButton(actionEvent: ActionEvent)
    {
        PurgeryConnector.courtOption(1)
    }

    fun withdrawButton(actionEvent: ActionEvent)
    {
        PurgeryConnector.courtOption(5)
    }

    fun pressCharges(actionEvent: ActionEvent)
    {
        PurgeryConnector.courtOption(6)
    }

    fun betPlaintiff(actionEvent: ActionEvent)
    {
        PurgeryConnector.courtOption(2)
    }

    fun betDef(actionEvent: ActionEvent)
    {
        PurgeryConnector.courtOption(3)
    }

    fun Abstain(actionEvent: ActionEvent)
    {
        PurgeryConnector.courtOption(4)
    }

    var dCoins = 0
    var pCoins = 0
    fun displayCoins(n: Int, array: ArrayList<ImageView>)
    {
        // Reset before changing.
        array.map { Platform.runLater { it.isVisible = false } }
        when
        {
            n <= 0 -> array.map { Platform.runLater { it.isVisible = false } }
            n == 1 -> Platform.runLater{array[4].isVisible = true}
            n == 2 -> Platform.runLater{array[0].isVisible = true; array[1].isVisible = true}
            n == 3 -> Platform.runLater{array[0].isVisible = true; array[4].isVisible = true; array[1].isVisible = true}
            n == 4 -> Platform.runLater{for (c in 0..3) array[c].isVisible = true}
            else -> array.map {Platform.runLater { it.isVisible = true }}
        }
    }



    /*-------------------------------------*/
    @FXML
    private lateinit var txt_card_mod_1: Text

    @FXML
    private lateinit var txt_card_mod_2: Text

    @FXML
    private lateinit var txt_card_mod_3: Text

    @FXML
    private lateinit var txt_card_mod_4: Text

    @FXML
    private lateinit var txt_card_mod_5: Text

    @FXML
    private lateinit var txt_card_mod_6: Text

    @FXML
    private lateinit var confirmLawSuitAnchor: AnchorPane

    @FXML
    private lateinit var txt_targetSue: Text

    @FXML
    private lateinit var anchorLaw2: AnchorPane

    @FXML
    private lateinit var img_law_1: ImageView

    @FXML
    private lateinit var img_law_2: ImageView


    @FXML
    private lateinit var hb_obj_1: AnchorPane

    @FXML
    private lateinit var hb_obj_2: AnchorPane

    @FXML
    private lateinit var hb_obj_3: AnchorPane


    @FXML
    private lateinit var player1displayAnchor: AnchorPane

    @FXML
    private lateinit var player2displayAnchor: AnchorPane

    @FXML
    private lateinit var player3displayAnchor: AnchorPane

    @FXML
    private lateinit var player4displayAnchor: AnchorPane

    @FXML
    private lateinit var player1TurnIndicator: Text

    @FXML
    private lateinit var player2TurnIndicator: Text

    @FXML
    private lateinit var player3TurnIndicator: Text

    @FXML
    private lateinit var player4TurnIndicator: Text


    @FXML
    private lateinit var anchor_login: AnchorPane

    @FXML
    private lateinit var username: TextField

    @FXML
    private lateinit var anchor_connecting: AnchorPane

    @FXML
    private lateinit var connecting: Text

    @FXML
    private lateinit var game: AnchorPane

    @FXML
    private lateinit var rect_sue: Rectangle

    @FXML
    private lateinit var txt_sue: Text

    @FXML
    private lateinit var rect_draw:Rectangle

    @FXML
    private lateinit var txt_draw:Text

    @FXML
    private lateinit var rect_objectives:Rectangle

    @FXML
    private lateinit var txt_objective: Text

    @FXML
    private lateinit var img_card1: ImageView

    @FXML
    private lateinit var img_card2: ImageView

    @FXML
    private lateinit var img_card3: ImageView

    @FXML
    private lateinit var img_card4: ImageView

    @FXML
    private lateinit var img_card5: ImageView

    @FXML
    private lateinit var img_card6: ImageView

    @FXML
    private lateinit var hb_card1:AnchorPane

    @FXML
    private lateinit var hb_card2:AnchorPane

    @FXML
    private lateinit var hb_card3:AnchorPane

    @FXML
    private lateinit var hb_card4:AnchorPane

    @FXML
    private lateinit var hb_card5:AnchorPane

    @FXML
    private lateinit var hb_drawncard:AnchorPane

    @FXML
    private lateinit var currentAction:Text

    @FXML
    private lateinit var p1rect:Rectangle

    @FXML
    private lateinit var p2rect:Rectangle

    @FXML
    private lateinit var p3rect:Rectangle

    @FXML
    private lateinit var p4rect: Rectangle

    @FXML
    private lateinit var txt_p1name: Text

    @FXML
    private lateinit var txt_p2name: Text

    @FXML
    private lateinit var txt_p3name: Text

    @FXML
    private lateinit var txt_p4name: Text

    @FXML
    private lateinit var img_obj_3_1: ImageView

    @FXML
    private lateinit var img_obj_3_2: ImageView

    @FXML
    private lateinit var img_obj_3_3: ImageView

    @FXML
    private lateinit var img_obj_1_1: ImageView

    @FXML
    private lateinit var img_obj_1_2: ImageView

    @FXML
    private lateinit var img_obj_1_3: ImageView
    @FXML
    private lateinit var img_obj_2_1: ImageView

    @FXML
    private lateinit var img_obj_2_2: ImageView
    @FXML
    private lateinit var img_obj_2_3: ImageView

    @FXML
    private lateinit var anchorObjective: AnchorPane

    @FXML
    private lateinit var anchorObjectiveAppear: AnchorPane

    @FXML
    private lateinit var plaintiff_name: Text

    @FXML
    private lateinit var defendant_name: Text

    @FXML
    private lateinit var txt_effectOverview: Text

    var plaintiffCoins = ArrayList<ImageView>()
    var defendantCoins = ArrayList<ImageView>()
    @FXML
    private lateinit var plaintiffCoin_5: ImageView
    @FXML
    private lateinit var plaintiffCoin_4: ImageView
    @FXML
    private lateinit var plaintiffCoin_3: ImageView
    @FXML
    private lateinit var plaintiffCoin_2: ImageView
    @FXML
    private lateinit var plaintiffCoin_1: ImageView



    @FXML
    private lateinit var defendantCoin_5: ImageView
    @FXML
    private lateinit var defendantCoin_4: ImageView
    @FXML
    private lateinit var defendantCoin_3: ImageView
    @FXML
    private lateinit var defendantCoin_2: ImageView
    @FXML
    private lateinit var defendantCoin_1: ImageView

    @FXML
    private lateinit var imgGuilty: ImageView

    @FXML
    private lateinit var imgNotGuilty: ImageView

    @FXML
    private lateinit var img_perjury: ImageView
}