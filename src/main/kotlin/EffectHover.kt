import javafx.scene.layout.AnchorPane

/**
 *
 */
class EffectHover
{
    fun get(): AnchorPane
    {
        val anchorPane = AnchorPane()
        anchorPane.minWidth = 200.0
        anchorPane.minHeight = 150.0
        anchorPane.prefHeight = 150.0
        anchorPane.prefWidth = 200.0
        anchorPane.layoutX = 100.0
        anchorPane.layoutY = 100.0
        AnchorPane.setRightAnchor(anchorPane, 5.0)
        AnchorPane.setLeftAnchor(anchorPane, 10.0)

        return anchorPane
    }
}